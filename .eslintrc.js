const path = require('path')

module.exports = {
  extends: [ 'airbnb' ], // A configuration file, once extended, can inherit all the traits of another configuration file
  env: { // An environment provides predefined global variables
    es2021: true,
    browser: true,
    jest: true,
  },
  settings: { // ESLint supports adding shared settings into configuration files. Plugins use settings to specify information that should be shared across all of its rules. You can add settings object to ESLint configuration file and it will be supplied to every rule being executed
    'import/resolver': {
      webpack: { // Setting for eslint-import-resolver-webpack
        config: path.resolve(__dirname, './bundler/webpack.dev.js'),
      },
      jest: { // Setting for eslint-import-resolver-jest
        jestConfigFile: path.resolve(__dirname, './jest.config.js'),
      }
    },
  },
  parserOptions: { // ESLint allows you to specify the JavaScript language options you want to support. By default, ESLint expects ECMAScript 5 syntax
    ecmaVersion: 2021, // To specify the version of ECMAScript syntax you want to use (5 by default)
    sourceType: 'module', // Set to 'script" (default) or "module" if your code is in ECMAScript modules
    ecmaFeatures: { // An object indicating which additional language features you'd like to use
      jsx: true,
    },
  },
  rules: {
    'react/jsx-filename-extension': [
      'error',
      {
        extensions: [ '.jsx', '.js' ],
      },
    ], // Define file extensions for react
    'no-async-promise-executor': 'off', // Enable set an async executor when you create a promise
    'array-bracket-spacing': [ 'error', 'always' ], // Enable set spacing at the beginning and end of an array
    'no-param-reassign': [ 'error', { props: false } ], // Enable reassignation of properties of a variable (for immer)
    'object-curly-newline': 0, // Disable new line after curly brace object
    'jsx-quotes': [ 'error', 'prefer-single' ], // Enable single quotes on react components
    'quotes': [ 'error', 'single', { allowTemplateLiterals: true } ], // Enable single quotes and template literals
    'eol-last': 0, // Disable validation of new line at the end of files
    'curly': [ 'error', 'multi' ], // Enable curly on multiline blocks
    'react/jsx-max-props-per-line': [ 2, { maximum: 1, when: 'always' } ], // Enable one prop per line in a react component
    'react/jsx-first-prop-new-line': 2, // Enable that first prop of a react component is placed in a new line
    'react/forbid-prop-types': [
      'error',
      {
        forbid: [ 'any', 'array' ],
        checkContextTypes: true,
        checkChildContextTypes: true,
      },
    ], // Enable type of proptypes
    'no-underscore-dangle': ['error', { 'allow': ['_sum', '_count', '_avg', '_min', '_max'] }]
  },
}
