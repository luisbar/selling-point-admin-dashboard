#### 0.21.3 (2022-01-22)

##### Code Style Changes

*  update bill styles ([e1e1e039](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/e1e1e0396948dc4b336560553b6604ac199c80e0))

#### 0.21.2 (2022-01-22)

##### Continuous Integration

*  update dockerfile ([88a5ddcc](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/88a5ddcc66c3698c69f931357c6b2f7bf2c283b3))
*  update .gitignore file ([a21ae8c3](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/a21ae8c3b18838faa6d281e348200e0890b498b2))
*  enable systemvars on dotenv webpack library ([555f7378](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/555f7378f36276b0c8cc114dbf8fe8082275522d))
*  add env var in order to skip env file ([4f83b935](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/4f83b9352310fdcaa820d1cc6555d698ca145d56))

##### Bug Fixes

*  update regex for creating and update a product, category and customer ([b50990b6](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/b50990b6be5580291bf7a228c70464cef3c78193))

##### Code Style Changes

*  change side menu class on tailwind config and update styles on home page ([a2d0393c](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/a2d0393ce91cddd14d29c1fab242bf385a7e04a9))

#### 0.21.1 (2021-12-31)

##### Bug Fixes

*  add entry and exit actions to chart machines ([34da813a](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/34da813a129a1e147c434bf3f47a58b04eb11dd3))

### 0.21.0 (2021-12-31)

##### Continuous Integration

*  update eslint rules ([f7fd3a8e](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/f7fd3a8ea0f50d99b5d09d0d70adf79a59dd5e33))

##### New Features

*  add charts to home page ([b3248833](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/b32488337f3fe486bb4879a3dd0877b1a96761ae))
*  create chart machines ([adb68cad](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/adb68cadc0f2fb57769f03de92a52d3aace6052f))
*  create chart component ([a8c92f2f](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/a8c92f2f35fe599a7ae8dcd03f743c0a344cb4ef))

##### Other Changes

*  install chartjs ([79c7a9ec](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/79c7a9ec7a5602159a5350e2d907d43804084ee8))
*  updating home page ([8d1a4e74](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/8d1a4e74b7535e0d0c3742654c34190aeb157e16))

##### Refactors

*  remove unused logout form child from sales, customers, products and categories machine ([e5edd87d](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/e5edd87d5dcca6caafbcfd7bd1fe293de4974541))
*  change product input by a simple input ([97d84570](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/97d84570031463965a4d930572df5148a05bcd33))
*  remove unused components ([dac18e00](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/dac18e00dc1b37ef2ce045ca7e25f419d1bf4dda))

### 0.20.0 (2021-12-30)

##### New Features

*  add create customer modal to create sale and update sale forms ([03f97017](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/03f97017853468126539ecd5297f531a0ee47a8d))
*  create page that simulates a modal for creating a customer ([9871d08a](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/9871d08a86f65eaae8b587f5b3f247dee95b3de6))
*  create modal template ([74578433](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/7457843336831abe7deb834c9dc2c51706ae2910))

##### Refactors

*  remove console log ([0b9ce6d3](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/0b9ce6d3dbd83f1eb88b2a2c303a8c21caebd2de))
*  change string keys ([5bb828ab](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/5bb828abeba6c029ff5fa860f530c3d4bb427b42))

#### 0.19.1 (2021-12-30)

##### Bug Fixes

*  add font sans style to each text components on bill component ([7081b44e](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/7081b44e4202437320ffa807ab36dc4387977c54))
*  add missing classname to table cell of header actions type ([9dac79fb](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/9dac79fbf7a9d7261096648356b0c9cd8f60644c))

### 0.19.0 (2021-12-30)

##### New Features

*  add bill component to views for creating and updating a sale ([284b12b8](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/284b12b81aed884a2162bddf0e4ebc89b79e0214))
*  create bill component in order to print a bill using react to print ([e4440828](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/e4440828b386d02f9e968674bc365880ea48bddb))
*  add values property to text component ([a8758a98](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/a8758a984be38f656195fffdc57b49c564c4b281))

##### Other Changes

*  install react to print ([71bcffcd](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/71bcffcdb3cbd2d103c15436e28fbadd2255fd68))

### 0.18.0 (2021-12-29)

##### New Features

*  change customer dropdown by debounced datalist on pages for creating and updating a sale ([9a90f95a](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/9a90f95ae13e66815536d63c99b095562acc2f07))
*  create debounced datalist component ([d9b2f2ec](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/d9b2f2eca3efd5ed6a93ad13ae48a6a34aeb857c))

### 0.17.0 (2021-12-29)

##### New Features

*  add given money and change inputs to update sale form ([c692e10e](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/c692e10ed14a2846da25c4cb11870073a64f7a6c))
*  add given money and change inputs to create sale form ([cb845bb5](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/cb845bb520f7f3828729410eefddd2fd47c383ba))

### 0.16.0 (2021-12-28)

##### New Features

*  add confirmation dialog to sales page ([d709bab8](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/d709bab8ea1cc9e79171383d5bc3cae20b6925aa))
*  link machines for completing and canceling a sale to sale machine ([92bbe822](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/92bbe822e2c7b8019158b251f1f543dff32dd80f))
*  create confirmation dialog component ([ae1a2558](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/ae1a25583f7ecd9b6e87c0e4e73a6701ea54f144))
*  create machines for completing and canceling a sale ([697e3963](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/697e3963696ad96c20ae15d7e6d89ad35029a21d))
*  update table cell of actions type ([6e49b5e8](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/6e49b5e88e2aae70e319352ec39a406a4969a6d0))

##### Other Changes

*  update react icons ([b89af4f8](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/b89af4f8d8914723970d185d3b2c07c899293caa))

### 0.15.0 (2021-12-27)

##### New Features

*  create page for displaying detail about a sale ([8db460ec](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/8db460ec015cf17c59de2da4c1e350cd99bd319e))

##### Code Style Changes

*  update components for displaying details about product, category and customer ([c8cb95e3](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/c8cb95e38f3e0059a93dc39cb32c2018f3682dfa))

### 0.14.0 (2021-12-27)

##### New Features

*  create page for updating a sale ([07fe191c](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/07fe191c23450402f08cba5ef55d0c7f508c0978))
*  create form for updating a sale ([5732a878](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/5732a87892f325835cf472cf272ac7eb97149fda))
*  add validation for updating a sale ([155364ba](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/155364ba58a5b53c0d92f4310809e0d4653aa070))
*  redirect to items list after updating a category, product or customer ([552f4d70](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/552f4d705ed7a9cd71017d5930eb86d7ed146289))

### 0.13.0 (2021-12-23)

##### New Features

*  create page for creating a sale ([65411c07](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/65411c07082385a9bf30942aa72b1658a2f708f9))
*  create form for creating a sale ([000c811a](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/000c811a0f2088b8515081e5b46c981531b96427))
*  add validation for validating a sale ([b16895ab](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/b16895ab60c052af843d6b4d7261f59ab59f73bd))
*  create product inputs component ([a18e0060](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/a18e0060907727200fc34099679234baf90e6380))
*  create product input component ([e29a452d](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/e29a452d49793cf6498ff9c0c8c60afa33b8cf30))
*  add labeless property to input component ([cfeb21b9](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/cfeb21b9805d983664efd2207084c49202436510))
*  add onkeyup event to button component ([5c8897c3](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/5c8897c3f09dfe5aa9c84cfddfde408693bb6805))

##### Bug Fixes

*  fix url ([438d74c5](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/438d74c5592f73f1129c452577b35f3293214b42))

##### Other Changes

*  update commitizen config file ([693eb834](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/693eb834ffe73776caf7a7c289a4373358a83f73))
*  install ramda ([895ce992](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/895ce9922fa2aa2a87ae037902bc7b4acce8b63d))

##### Code Style Changes

*  fix update product form ([d181072e](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/d181072e94f2c610c765354f47e877480897110b))
*  fix update customer form ([5cfb8ee3](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/5cfb8ee3b911c12315a7628a02afc33ba872d6ab))
*  center checkbox of update category form component ([904c170c](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/904c170c91ad284a14472058409b72b5bc3e7cc0))
*  center text of table cell of text type ([6871d605](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/6871d605c88c04b671abbf801628767e641dde85))
*  add ring when dropdown is focused ([7f84d870](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/7f84d8703495e2de8e69eda2b76aa6068c0e9fc5))
*  add ring when the checkbox is focused ([46123fef](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/46123fef738f8e51a72de38c619a043260738b13))

### 0.12.0 (2021-12-20)

##### New Features

*  add filter, table and paginator to sales page ([db37bc0d](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/db37bc0d42278e0f7aa65fc9b302fa5b42afb70e))
*  add value property to login form inputs ([6e8e8d3f](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/6e8e8d3f1193a41a96001cb2bdef6cc335801bc8))

#### 0.11.1 (2021-12-18)

##### Bug Fixes

*  add fallback to details view when a data is falsy ([174c202a](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/174c202a6b85079c00d758dddcd3159e4bb07239))
*  make optional email and nit in order to create a customer ([330f6a18](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/330f6a1881c4be4b4d781be0377cffa90ee591a2))

### 0.11.0 (2021-12-17)

##### New Features

*  create view for updating a customer ([1f4d661c](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/1f4d661c236b9b1a751dadd4039fd195b6607893))
*  create form for updating a customer ([bf414389](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/bf414389ac1b2601955381f57b923b5b83d38127))
*  add validation for updating a customer ([978d83ff](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/978d83ff05798d0c5498987d77e974549eade112))

### 0.10.0 (2021-12-17)

##### New Features

*  create page for viewing a customer ([b042bf3d](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/b042bf3daf78904128c6866ccb67ba111f60039e))

### 0.9.0 (2021-12-17)

##### New Features

*  create page for creating a customer ([dd571de8](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/dd571de80db3706b05edcf3da321e6da9b6ea801))
*  create form for creating a customer ([7010c8e1](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/7010c8e1ae8ef90fbc90fe84a369e698f3353168))
*  add validation for creating a customer ([59dad27c](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/59dad27c6c7195baa22791b229eacc3486501418))

### 0.8.0 (2021-12-17)

##### New Features

*  add filter, table and paginator to customers page ([eee55308](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/eee55308eced8cde8b2f0501b53ffcf6cc056548))

#### 0.7.1 (2021-12-17)

##### New Features

*  add category to products filter form ([8b2650eb](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/8b2650eb470d6a54f8fd442f6570ca1c34964ec8))
*  add category name to products table ([aacde8bb](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/aacde8bb3447f0a501d69713900e9538c0f74d7f))

##### Bug Fixes

*  pass value to dropdown language ([3a658b2c](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/3a658b2c32892c414ea6e1bda50fdcecb3e8df8b))

### 0.7.0 (2021-12-17)

##### New Features

*  create update product page ([6ef03861](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/6ef0386150f7336312bdf2ef889c3446227c5d0d))
*  create update product form ([f6c6cda4](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/f6c6cda48f12b5e1ba6e9ad82af269e89d57e8e1))
*  add validation for updating a product ([7007d50b](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/7007d50b19362334cb60a6e6e789b77aa48c6291))

### 0.6.0 (2021-12-16)

##### New Features

*  create the view product page ([43badfd9](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/43badfd936f695e5565796e6c2e72409b4750c9b))
*  include category when getting products ([08b051d2](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/08b051d2f7b9af92603180ebc2810f3075dc69c9))

##### Refactors

*  change the name of the category detail component to detail ([71038122](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/71038122c23997b837cbc37950668fcc2d14ca8e))

### 0.5.0 (2021-12-16)

##### New Features

*  link the page for creating a product to the products page ([d7aad7da](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/d7aad7da6ec9f1ea327fe42acac10831efa90242))
*  create the page for creating a product ([e81cc4b8](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/e81cc4b895b584873c074f7a55ab5cebe19d2573))
*  create the form for creating a product ([33b0b79e](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/33b0b79e2eb07699db5588c95e3f595b56288f29))
*  add validation for creating a product and update the schema validator ([9bf93f34](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/9bf93f34bc371e3b80ae72b8585853236a3550f4))
*  transform input, checkbox and dropdown to controlled components ([ef02ae73](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/ef02ae73944d8c2f25dff3ea838f6d872c50c654))

##### Refactors

*  move the category detail component to organisms folder ([91ec532a](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/91ec532a78e887168d2a88c924abeb3faf2113ff))
*  change name of previous data to current data on filter form component ([3f45dd40](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/3f45dd40ebdf70b5f2c45e8839a8e7550e5f6d26))
*  change error declaration from null to an empty object for the logout form machine ([d22d0474](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/d22d0474652502965259645f4a955e7ad60f3f3c))

##### Code Style Changes

*  add fix height to button ([ee0ea127](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/ee0ea1276cf22707ae8cb5da8ccd7de7e2e2e18c))

### 0.4.0 (2021-12-16)

##### New Features

*  add filter, paginator and table components to products page ([a5cee661](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/a5cee66184577e19579d21a59c4c85f0d7e0914a))

##### Bug Fixes

*  convert to string before sending to text component for table cell of text type ([c16fc215](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/c16fc2156bdf998efddacd6c7ff6d037676668bb))

### 0.3.0 (2021-12-15)

##### New Features

*  link update category page to categories page ([73dd3a97](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/73dd3a970823c6eec2ad2c85a0bc3b5c4d6bd380))
*  create update category view ([86c0baaf](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/86c0baafcc757f48424eae05620d854e9dc8e7de))
*  create update category form ([f5e0667c](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/f5e0667c3730df5d47d48965f44a989e66025257))
*  add validation for updating a category ([fe3b6f9a](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/fe3b6f9ae43e2be265d6d320bf907671e6e8b0e4))
*  add deault value property to input component ([37677af9](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/37677af962caa518a333c1d4d5b768a1b62cdbaa))
*  add default value, on change and id properties to checkbox component ([f3ccc93c](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/f3ccc93c0b04d7ceb4e3d8ea77bd6feed6d90415))

##### Bug Fixes

*  update keys of actions cell type ([6460aeac](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/6460aeacf8fab122e7e1eefb6de5c38ba2669fcc))
*  update keys of category detail items ([63ea3d36](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/63ea3d36bf284c8e4db3779c99eec1459544f245))
*  add missing cell types to table component prop types ([f26f74e5](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/f26f74e57b6a04142c0ba6e5fec9b2d7e51a7b13))

##### Code Style Changes

*  align language dropdown and checkbox theme to filter form ([8c9ca210](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/8c9ca21059be1295df305eda08ceacf15e146719))

#### 0.2.1 (2021-12-15)

##### Bug Fixes

*  check if token exists before decoding it ([5c023c90](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/5c023c909c7b34281d49d9e65865caf8ae0885fc))

### 0.2.0 (2021-12-15)

##### New Features

*  link view category page to categories page ([a1efdc6f](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/a1efdc6fe3d29e3a036e8190bc3165ff224e10d2))
*  create view category page ([e912695f](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/e912695f31dad235b2c6ff470b05c848183415ee))
*  create category detail component ([cc3e313c](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/cc3e313cabcb1216986ffe9dca588e84d349dab0))
*  create tiny card component ([869f1858](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/869f1858b76331ae4840178e6a3fea9698199e2b))
*  update columns shown on categories table ([c30078ba](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/c30078ba56d43142a9206b491923733648c67b6b))
*  add boolean type to table cell and fix style of actions type of table cell ([fa3c1fd1](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/fa3c1fd1a1bf2659d9276bd703d4097b7f137f07))
*  add action for showing backend errors on input during category creation ([7141e08f](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/7141e08fed11e9635a98420a7b70ef3f0fd8fe74))
*  update auth guard for validating jwt expiration ([87dcd4f5](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/87dcd4f52806bd117b640c07c91ad7f05593fdff))
*  change cell type for created at column ([36a651cb](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/36a651cb01a8e303eb17fea8588702f837b4b58a))
*  add date type to table cell ([f6423e51](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/f6423e51d0f3a92251952acd398cbee1eb2f116e))

##### Bug Fixes

*  remove enabled property from where clause during category fetch ([720efa28](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/720efa283c58082bb0c1190f75ccad37e2eb60ba))
*  improve validation for creating a category ([4e9b688d](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/4e9b688ddf01210f3fcad013eda63225a63d42c1))
*  redirect to login when receive 403 error ([dd00628c](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/dd00628c3941cb95d03d7f911c196c205b02c27b))

### 0.1.0 (2021-12-14)

##### New Features

*  add version to side menu ([bfa9359a](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/bfa9359acfd3aaf0e107cb213fce2a543bcdb861))
*  create page for creating a category ([1a864b09](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/1a864b09cc6e41eb75d61ad2075b32656afd5414))
*  create form for creating a category ([ce08c003](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/ce08c003e42964157fa00bd4e68551d51c550ca7))
*  add validation for creating a category ([3bc0ad27](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/3bc0ad2700d97d80d26a9ae2b4cc05c3ace9cf6f))
*  add filter form, table and paginator to categories page ([720a3f8f](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/720a3f8f565f17dbee81fc1280cd59103720f8f5))
*  add validator for filter form data ([b72c4d12](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/b72c4d1291e2c4d14b6c80d7dd83b10e38dc34a0))
*  add token to axios headers ([61670fe1](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/61670fe117f3dd9a3046a7e7f078c52cd46f5df8))
*  add filter form component ([122b9b19](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/122b9b1944235f758624332a6f52312bb4667ab6))
*  add default value to dropdown component ([c57b059d](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/c57b059dd9ce1a092277105ce0ca184143fbb110))
*  add categories, products, customers and orders pages ([866b9c13](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/866b9c1395dd300d4a062081af882bf94e109829))
*  connect the dashboard with the auth server in order to login ([c38f8957](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/c38f8957538ef87d01edd6856365986891bf355a))

##### Bug Fixes

*  get enabled categories and order them by id ([2520adff](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/2520adff8bc5ce0e6e2737eb2340a8f38d037e62))
*  change default page size property of filter form to number ([1934f0e7](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/1934f0e715772e04078c8939aed8e0813afcda57))
*  convert page size to number ([f4de774a](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/f4de774a368f4fd8b8c1b6fc955d642777d16571))
*  translate keys of dropdown options ([f77e3c44](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/f77e3c447e2701d9b46c37b5d899fc3249d7c8da))

##### Other Changes

*  update favicon ([1f5be6e4](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/1f5be6e4a9a383324c8193b3e98765274b865765))
*  update logo ([618616e9](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/618616e9fc127f191a2bc7f29a1d6695a4e6551d))
*  change colors ([5fd719f6](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/5fd719f67b76b5132f0e4b0f1edfad063463b3ce))

##### Refactors

*  remove unused action on login machine ([f8e342c9](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/f8e342c90080b58fa42faf0f7566b07887f2c510))
*  change undefined per null ([81766dbd](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/81766dbd020f0d356f91048a1668bbb92ac078b7))
*  remove unused pages ([de6656cd](https://gitlab.com/luisbar/selling-point-admin-dashboard/commit/de6656cd94220cdbe0476e5845d3d01043c64a59))

