const path = require('path');
const { merge } = require('webpack-merge');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const commonWebPackConfiguration = require('./webpack.common');
const Dotenv = require('dotenv-webpack');
const plugins = [
  new CopyWebpackPlugin({
    patterns: [
      {
        from: path.resolve(__dirname, '../src/serviceWorker.js'),
        to: 'serviceWorker.js',
      },
    ],
  }),
  new Dotenv({
    path: path.join(__dirname, '../.env.production'),
    safe: true,
  }),
]
if (process.env.IGNORE_ENV_FILE) plugins = [
  new CopyWebpackPlugin({
    patterns: [
      {
        from: path.resolve(__dirname, '../src/serviceWorker.js'),
        to: 'serviceWorker.js',
      },
    ],
  }),
  new Dotenv({
    systemvars: true,
  }),
]

module.exports = merge(commonWebPackConfiguration, {
  entry: [path.join(__dirname, '../src/indexWithServiceWorker.js')],
  mode: 'production',
  plugins,
});
