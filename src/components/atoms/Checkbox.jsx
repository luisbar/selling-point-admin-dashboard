import React, { useEffect } from 'react';
import { useActor, useMachine } from '@xstate/react';
import { FormattedMessage } from 'react-intl';
import propTypes from 'prop-types';

const Checkbox = ({ label, data, className, machine, test, onChange, value, id }) => {
  const actor = test ? useMachine(machine) : useActor(machine);
  const [ state, send ] = actor;

  const onClick = () => {
    send({
      data,
      type: 'TOGGLE',
    });
  };

  useEffect(() => {
    if ((value === true && state.matches('unchecked'))
        || (value === false && state.matches('checked'))
    ) send({
      data,
      type: 'TOGGLE',
    });
  }, []);

  useEffect(() => {
    if (state.matches('checked')) onChange({ target: { id, value: true } });
    if (state.matches('unchecked')) onChange({ target: { id, value: false } });
  }, [ state.value ]);

  return (
    <button
      type='button'
      data-testid='checkbox-container'
      className={`flex flex-col items-center cursor-pointer focus:ring-1 focus:ring-primary rounded-md p-1 ${className}`}
      onClick={onClick}
    >
      <span className='dark:text-accent-200'>
        <FormattedMessage id={label} />
      </span>
      <input
        id={id}
        type='checkbox'
        className='hidden'
        value={value}
      />
      <div
        data-testid='checkbox-child-3'
        className={`w-8 h-4 rounded-md duration-500 ${state.matches('checked') ? 'bg-accent-100' : 'bg-dark-300'}`}
      >
        <div
          data-testid='checkbox-child-3.1'
          className={`bg-light-100 w-4 h-4 rounded-full duration-500 ${state.matches('checked') ? 'transform translate-x-full' : ''}`}
        />
      </div>
    </button>
  );
};

Checkbox.propTypes = {
  label: propTypes.string.isRequired,
  data: propTypes.object.isRequired,
  className: propTypes.string,
  machine: propTypes.object.isRequired,
  test: propTypes.bool,
  onChange: propTypes.func,
  value: propTypes.bool.isRequired,
  id: propTypes.string,
};

Checkbox.defaultProps = {
  className: '',
  test: false,
  onChange: () => {},
  id: '',
};

export default Checkbox;
