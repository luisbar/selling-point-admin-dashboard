import { useActor } from '@xstate/react';
import React, { useState, useEffect } from 'react';
import propTypes from 'prop-types';
import Text from '@atoms/Text';
import { useGlobalSpinnerContext } from '@atoms/GlobalSpinner';
import { useGlobalAlertContext } from '@molecules/GlobalAlert';

const Datalist = ({ machine, debouncingTime, className, text, input }) => {
  const [ state, send ] = useActor(machine);
  const toggleSpinner = [ ...useGlobalSpinnerContext() ].pop();
  const toggleAlert = [ ...useGlobalAlertContext() ].pop();
  const [ timer, setTimer ] = useState(null);
  const [ error, setError ] = useState(null);

  const renderText = () => {
    if (error) return (
      <Text
        label={error}
        className='text-red-300'
      />
    );

    return (
      <Text
        className={text.className}
        label={text.label}
        as={text.as}
      />
    );
  };

  const renderOptions = () => state.context.options.map(({ key, value }) => (
    <option
      key={key}
    >
      {value}
    </option>
  ));

  const getValue = () => {
    const matchedOption = state.context.options
      .filter((option) => option.key === input.value);
    return matchedOption.length ? matchedOption[0].value : input.value;
  };

  const onChange = ({ target }) => {
    const optionSelected = state.context.options
      .filter((option) => option.value === target.value)[0];

    input.onChange({
      id: target.id,
      key: optionSelected ? optionSelected.key : target.value,
      value: optionSelected ? optionSelected.value : target.value,
    });

    if (timer) clearTimeout(timer);
    if (!target.value) send({ type: 'CLEAN_OPTIONS' });
    if (target.value) setTimer(() => setTimeout(() => send({
      type: 'PROCESS',
      data: {
        filter: target.value,
      },
    }), debouncingTime));
  };

  useEffect(() => {
    if (state.matches('processing')) toggleSpinner({ type: 'ENABLE' });
    if (state.matches('failed') || state.matches('finished')) toggleSpinner({ type: 'DISABLE' });
    if (state.matches('failed')) toggleAlert({
      type: 'ENABLE',
      data: { type: 'error', message: state.context.error.message },
    });
  }, [ state.value ]);

  useEffect(() => {
    if (!input.value) send({ type: 'CLEAN_OPTIONS' });
  }, [ input.value ]);

  useEffect(() => {
    if (state.matches('showingError')) setError(() => state.context.error);
    if (state.matches('cleaningError')) setError(() => null);
  }, [ state ]);

  useEffect(() => {
    if (input.value) send({ type: 'PROCESS', data: { filter: input.value } });
  }, []);

  return (
    <div
      className={`flex flex-col ${className}`}
    >
      {renderText()}
      <input
        disabled={state.matches('disabled')}
        className='bg-gray-100 border-0 rounded-md disabled:cursor-not-allowed focus:ring-primary'
        id={input.id}
        list={`${input.id}-list`}
        onChange={onChange}
        type='text'
        value={getValue()}
        autoComplete='off'
      />
      <datalist
        id={`${input.id}-list`}
      >
        {renderOptions()}
      </datalist>
    </div>
  );
};

Datalist.propTypes = {
  machine: propTypes.object.isRequired,
  debouncingTime: propTypes.number,
  className: propTypes.string,
  text: propTypes.shape(Text.propTypes),
  input: propTypes.shape({
    id: propTypes.string.isRequired,
    value: propTypes.string.isRequired,
    onChange: propTypes.func.isRequired,
  }).isRequired,
};

Datalist.defaultProps = {
  debouncingTime: 1000,
  className: '',
  text: {
    label: '',
    className: '',
  },
};

export default Datalist;