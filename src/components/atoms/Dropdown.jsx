import React, { useState, useEffect } from 'react';
import propTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import Text from '@atoms/Text';
import { useActor } from '@xstate/react';

const Dropdown = ({ machine, className, text, select }) => {
  const [ state ] = useActor(machine);
  const [ error, setError ] = useState(null);

  useEffect(() => {
    if (state.matches('showingError')) setError(() => state.context.error);
    if (state.matches('cleaningError')) setError(() => null);
  }, [ state ]);

  const renderOptions = () => select.options.map(({ key, value }) => (
    <FormattedMessage
      key={key}
      id={value}
      defaultMessage={value}
    >
      {
        (formattedValue) => (
          <FormattedMessage
            key={key}
            id={key}
            defaultMessage={key}
          >
            {
                (formattedKey) => (
                  <option value={formattedKey}>{formattedValue}</option>
                )
              }
          </FormattedMessage>
        )
      }
    </FormattedMessage>
  ));

  return (
    <div
      className={`flex flex-col ${className}`}
    >
      {
        error
          ? (
            <Text
              label={error}
              className='text-red-300'
            />
          )
          : (
            <Text
              text={text.className}
              label={text.label}
              as={text.as}
            />
          )
      }
      <select
        id={select.id}
        disabled={state.matches('disabled')}
        className={`bg-gray-100 border-none rounded-md focus:ring-light disabled:cursor-not-allowed focus:ring-primary ${select.className}`}
        onChange={select.onChange}
        value={select.value}
      >
        {renderOptions()}
      </select>
    </div>
  );
};

Dropdown.propTypes = {
  machine: propTypes.object.isRequired,
  className: propTypes.string,
  select: propTypes.shape({
    id: propTypes.string,
    className: propTypes.string,
    options: propTypes.arrayOf(
      propTypes.shape({
        key: propTypes.string.isRequired,
        value: propTypes.string.isRequired,
      }).isRequired,
    ).isRequired,
    onChange: propTypes.func.isRequired,
    value: propTypes.string,
  }).isRequired,
  text: propTypes.shape(Text.propTypes).isRequired,
};

Dropdown.defaultProps = {
  className: '',
};

export default Dropdown;