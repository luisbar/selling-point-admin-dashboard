import React from 'react';
import propTypes from 'prop-types';

const Image = ({ src, className }) => (
  <img
    className={`object-contain ${className}`}
    src={src}
    alt={src.split('/').pop()}
  />
);

Image.propTypes = {
  src: propTypes.string.isRequired,
  className: propTypes.string,
};

Image.defaultProps = {
  className: '',
};

export default Image;
