import React, { useEffect, useState } from 'react';
import propTypes from 'prop-types';
import Text from '@atoms/Text';
import { useActor } from '@xstate/react';

const Input = ({ machine, className, input, text, labeless }) => {
  const [ state ] = useActor(machine);
  const [ error, setError ] = useState(null);

  const renderText = () => {
    if (error) return (
      <Text
        label={error}
        className='text-red-300'
      />
    );
    if (!labeless) return (
      <Text
        className={text.className}
        label={text.label}
        as={text.as}
      />
    );

    return null;
  };

  useEffect(() => {
    if (state.matches('showingError')) setError(() => state.context.error);
    if (state.matches('cleaningError')) setError(() => null);
  }, [ state ]);

  return (
    <div
      className={`flex flex-col ${className}`}
    >
      {renderText()}
      <input
        disabled={state.matches('disabled')}
        className={`bg-gray-100 border-0 rounded-md disabled:cursor-not-allowed focus:ring-primary ${input.className}`}
        type={input.type}
        onChange={input.onChange}
        id={input.id}
        value={input.value}
      />
    </div>
  );
};

Input.propTypes = {
  machine: propTypes.object.isRequired,
  className: propTypes.string,
  labeless: propTypes.bool,
  input: propTypes.shape({
    type: propTypes.oneOf([ 'text', 'password', 'email', 'number', 'url', 'date', 'datetime-local', 'month', 'week', 'time', 'search', 'tel', 'checkbox', 'radio' ]).isRequired,
    className: propTypes.string,
    onChange: propTypes.func.isRequired,
    id: propTypes.string.isRequired,
    value: propTypes.string.isRequired,
  }).isRequired,
  text: propTypes.shape(Text.propTypes),
};

Input.defaultProps = {
  className: '',
  labeless: false,
  text: {
    label: '',
    className: '',
  },
};

export default Input;