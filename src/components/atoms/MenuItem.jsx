import React from 'react';
import Text from '@atoms/Text';
import propTypes from 'prop-types';
import { useNavigation } from 'react-navi';

const MenuItem = ({ path, label, className }) => {
  const navigation = useNavigation();

  const onClick = () => {
    navigation.navigate(path);
  };

  return (
    <button
      type='button'
      className={`active:bg-indigo-500 cursor-pointer row-start-2 pl-5 grid justify-start content-center ${navigation.lastHandledLocation.pathname === path ? 'bg-primary-light' : ''} ${className}`}
      onClick={onClick}
    >
      <Text
        className='text-white'
        label={label}
      />
    </button>
  );
};

MenuItem.propTypes = {
  path: propTypes.string.isRequired,
  label: propTypes.string.isRequired,
  className: propTypes.string,
};

MenuItem.defaultProps = {
  className: '',
};

export default MenuItem;