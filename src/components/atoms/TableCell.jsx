import React from 'react';
import * as icons from 'react-icons/bs';
import Text from '@atoms/Text';
import propTypes from 'prop-types';
import moment from 'moment';

const getValueUsingDotNotation = ({ columnKey, row }) => columnKey.split('.').reduce((accumulator, item) => accumulator[item], row);

const onClickAction = ({ row, onClickItem }) => () => {
  onClickItem(row);
};

const onClickHeaderAction = ({ onClickItem }) => () => {
  onClickItem();
};

const Cell = {
  header: ({ columnIndex, label }) => (
    <Text
      className={`row-start-2 col-start-${columnIndex + 1} self-center justify-self-center`}
      label={label}
      as='h6'
    />
  ),
  text: ({ rowIndex, columnIndex, row, columnKey }) => (
    <Text
      className={`row-start-${rowIndex + 1} col-start-${columnIndex + 1} self-center justify-self-center text-center p-2`}
      label={String(getValueUsingDotNotation({ columnKey, row }))}
    />
  ),
  boolean: ({ rowIndex, columnIndex, row, columnKey }) => {
    const Icon = getValueUsingDotNotation({ columnKey, row }) ? icons.BsCheck : icons.BsX;
    return (
      <Icon
        className={`row-start-${rowIndex + 1} col-start-${columnIndex + 1} self-center justify-self-center ${getValueUsingDotNotation({ columnKey, row }) ? 'text-success' : 'text-error'} text-2xl`}
        key={`action-${rowIndex}`}
      />
    );
  },
  date: ({ rowIndex, columnIndex, row, columnKey, format }) => (
    <Text
      className={`row-start-${rowIndex + 1} col-start-${columnIndex + 1} self-center justify-self-center`}
      label={moment(getValueUsingDotNotation({ columnKey, row })).format(format)}
    />
  ),
  actions: ({ rowIndex, columnIndex, row, actions }) => (
    <div
      className={`row-start-${rowIndex + 1} col-start-${columnIndex + 1} self-center justify-self-center flex flex-row`}
    >
      {
        actions.map(({ icon, onClickItem, className }) => {
          const Icon = icons[icon];
          return (
            <Icon
              className={`cursor-pointer text-accent-100 mx-2 ${className}`}
              key={`action-${icon}-${rowIndex}`}
              onClick={onClickAction({ row, onClickItem })}
            />
          );
        })
      }
    </div>
  ),
  headerActions: ({ headerActions, columnsCount }) => (
    <div
      className={`row-start-1 col-start-1 col-end-${columnsCount} self-center justify-self-end mr-5 text-2xl`}
    >
      {
        headerActions.map(({ icon, onClickItem, className }) => {
          const Icon = icons[icon];
          return (
            <Icon
              className={`cursor-pointer text-accent-100 ${className}`}
              key={`headerActions-${icon}`}
              onClick={onClickHeaderAction({ onClickItem })}
            />
          );
        })
      }
    </div>
  ),
};

Cell.header.propTypes = {
  columnIndex: propTypes.number.isRequired,
  label: propTypes.string.isRequired,
};

Cell.text.propTypes = {
  rowIndex: propTypes.number.isRequired,
  columnIndex: propTypes.number.isRequired,
  row: propTypes.string.isRequired,
  columnKey: propTypes.string.isRequired,
};

Cell.boolean.propTypes = {
  rowIndex: propTypes.number.isRequired,
  columnIndex: propTypes.number.isRequired,
  row: propTypes.string.isRequired,
  columnKey: propTypes.string.isRequired,
};

Cell.date.propTypes = {
  rowIndex: propTypes.number.isRequired,
  columnIndex: propTypes.number.isRequired,
  row: propTypes.string.isRequired,
  columnKey: propTypes.string.isRequired,
  format: propTypes.string.isRequired,
};

Cell.actions.propTypes = {
  rowIndex: propTypes.number.isRequired,
  columnIndex: propTypes.number.isRequired,
  row: propTypes.string.isRequired,
  actions: propTypes.arrayOf(propTypes.shape({
    icon: propTypes.string,
    onClickItem: propTypes.func,
    className: propTypes.string,
  })).isRequired,
};

Cell.headerActions.propTypes = {
  headerActions: propTypes.arrayOf(propTypes.shape({
    icon: propTypes.string,
    onClickItem: propTypes.func,
    className: propTypes.string,
  })).isRequired,
  columnsCount: propTypes.number.isRequired,
};

const TableCell = ({ type, ...others }) => {
  const renderCell = Cell[type];
  return renderCell({ ...others });
};

TableCell.propTypes = {
  type: propTypes.oneOf([ 'header', 'text', 'actions', 'headerActions', 'date', 'boolean' ]),
};

export default TableCell;