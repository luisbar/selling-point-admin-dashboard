import React from 'react';
import { FormattedMessage } from 'react-intl';
import propTypes from 'prop-types';
import unified from 'unified';
import markdown2remark from 'remark-parse';
import remark2react from 'remark-react';

const Text = ({ as, label, className, values }) => {
  const Component = as;

  if (as === 'md') return (
    <div
      className={className}
    >
      <FormattedMessage
        id={label}
        defaultMessage={label}
      >
        {
            ([ labelFormatted ]) => unified()
              .use(markdown2remark)
              .use(remark2react)
              .processSync(labelFormatted).result
          }
      </FormattedMessage>
    </div>
  );

  return (
    <Component
      className={className}
    >
      <FormattedMessage
        id={label}
        defaultMessage={label}
        values={values}
      />
    </Component>
  );
};

Text.propTypes = {
  as: propTypes.oneOf([ 'span', 'p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'md' ]),
  label: propTypes.string.isRequired,
  className: propTypes.string,
  values: propTypes.object,
};

Text.defaultProps = {
  as: 'span',
  className: '',
  values: {},
};

export default Text;