import React, { forwardRef } from 'react';
import moment from 'moment';
import Text from '@atoms/Text';
import propTypes from 'prop-types';

const Bill = forwardRef(({ data }, ref) => (
  <div
    className='hidden'
  >
    <div
      ref={ref}
      className='fixed inset-0'
    >
      <Text
        className='block text-center font-semibold'
        as='h5'
        label='bill.txt1'
      />
      <Text
        className='block font-sans font-semibold'
        as='span'
        label='bill.txt2'
        values={{ time: moment().format('HH:mm:ss') }}
      />
      <Text
        className='block font-sans font-semibold'
        as='span'
        label='bill.txt3'
        values={{ customerName: data.customerName }}
      />
      <div
        className='h-1 w-full bg-dark-100'
      />
      <br />
      {
          data.sale
          && data.sale.selectedProducts.map((product) => (
            <div
              key={product.id}
              className='h-auto'
            >
              <Text
                className='block font-sans font-semibold'
                as='span'
                label='bill.txt4'
                values={{ productName: product.name }}
              />
              <Text
                className='block font-sans font-semibold'
                as='span'
                label='bill.txt5'
                values={{ productPrice: product.price }}
              />
              <Text
                className='block font-sans font-semibold'
                as='span'
                label='bill.txt6'
                values={{ productQuantity: product.quantity }}
              />
              <Text
                className='block font-sans font-semibold'
                as='span'
                label='bill.txt7'
                values={{ productSubtotal: product.subtotal }}
              />
              <br />
            </div>
          ))
        }
      <div
        className='h-1 w-full bg-dark-100'
      />
      <Text
        className='block font-sans font-semibold'
        as='span'
        label='bill.txt8'
        values={{ total: data.sale ? data.sale.total : 0 }}
      />
    </div>
  </div>
));

Bill.propTypes = {
  data: propTypes.shape({
    customerName: propTypes.string,
    sale: propTypes.shape({
      selectedProducts: propTypes.arrayOf(propTypes.shape({
        id: propTypes.string,
        name: propTypes.string,
        price: propTypes.number,
        quantity: propTypes.string,
        subtotal: propTypes.number,
      })),
      total: propTypes.number,
    }),
  }),
};

Bill.defaultProps = {
  data: {},
};

export default Bill;