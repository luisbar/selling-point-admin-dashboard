import React, { useEffect, useState } from 'react';
import * as charts from 'react-chartjs-2';
import propTypes from 'prop-types';
import { useActor } from '@xstate/react';
import Text from '@atoms/Text';
import Input from '@atoms/Input';
import Button from '@atoms/Button';
import moment from 'moment';
import { useGlobalSpinnerContext } from '@atoms/GlobalSpinner';
import { useGlobalAlertContext } from '@molecules/GlobalAlert';
import {
  Chart as ChartJS,
  ArcElement,
  Tooltip,
  Legend,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
} from 'chart.js';

ChartJS.register(
  ArcElement,
  Tooltip,
  Legend,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
);

const Chart = ({ machine, type, className, text, period }) => {
  const [ state, send ] = useActor(machine);
  const toggleSpinner = [ ...useGlobalSpinnerContext() ].pop();
  const toggleAlert = [ ...useGlobalAlertContext() ].pop();
  const [ data, setData ] = useState({
    startDate: moment().startOf(period).format('YYYY-MM-DD'),
    endDate: moment().endOf(period).format('YYYY-MM-DD'),
  });
  const Chartjs = charts[type];

  const onChangeData = ({ target }) => {
    setData((currentData) => ({
      ...currentData,
      [target.id]: target.value,
    }));
  };

  useEffect(() => {
    if (state.matches('processing')) toggleSpinner({ type: 'ENABLE' });
    if (state.matches('finished')) toggleSpinner({ type: 'DISABLE' });
    if (state.matches('failed')) toggleSpinner({ type: 'DISABLE' });
    if (state.matches('failed')) toggleAlert({ type: 'ENABLE', data: { type: 'error', message: state.context.error.message } });
  }, [ state.value ]);

  useEffect(() => {
    send({
      type: 'PROCESS',
      data,
    });
  }, []);

  return (
    <div
      className={`grid grid-rows-6 grid-cols-3 gap-5 ${className}`}
    >
      <Text
        as={text.as}
        className={`row-start-1 col-start-1 col-span-3 self-center ${text.className}`}
        label={text.label}
      />
      <Input
        className='row-start-2 col-start-1 col-span-1 self-end'
        machine={state.children.startDateInput}
        text={{
          label: 'chart.txt1',
        }}
        input={{
          type: 'date',
          id: 'startDate',
          value: data.startDate,
          onChange: onChangeData,
        }}
      />
      <Input
        className='row-start-2 col-start-2 col-span-1 self-end'
        machine={state.children.endDateInput}
        text={{
          label: 'chart.txt2',
        }}
        input={{
          type: 'date',
          id: 'endDate',
          value: data.endDate,
          onChange: onChangeData,
        }}
      />
      <Button
        className='row-start-2 col-start-3 col-span-1 self-end'
        machine={state.children.filterButton}
        text={{
          label: 'chart.txt3',
        }}
        data={{
          type: 'PROCESS',
          data,
        }}
      />
      <div
        className='row-start-3 row-span-4 col-start-1 col-span-3'
      >
        <Chartjs
          options={{
            responsive: true,
            maintainAspectRatio: false,
            ...state.context.options,
          }}
          data={state.context.data}
        />
      </div>
    </div>
  );
};

Chart.propTypes = {
  machine: propTypes.object.isRequired,
  type: propTypes.oneOf([
    'Bar',
    'Bubble',
    'Chart',
    'Doughnut',
    'Line',
    'Pine',
    'PolarArea',
    'Radar',
    'Scatter',
    'Pie',
  ]).isRequired,
  className: propTypes.string,
  text: propTypes.shape(Text.propTypes).isRequired,
  period: propTypes.oneOf([ 'day', 'week', 'month', 'year' ]).isRequired,
};

Chart.defaultProps = {
  className: '',
};

export default Chart;