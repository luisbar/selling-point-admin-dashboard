import React from 'react';
import Text from '@atoms/Text';
import Button from '@atoms/Button';
import propTypes from 'prop-types';
import { IoMdClose } from 'react-icons/io';

const ConfirmationDialog = ({ message, button, onClose }) => (
  <div
    className='fixed inset-0 bg-dark-200 bg-opacity-50 overflow-y-auto h-full w-full flex flex-col items-center justify-center'
  >
    <div
      className='relative grid grid-rows-3 grid-col-4 gap-5 bg-light-100 rounded-md p-5'
    >
      <IoMdClose
        className='absolute top-1 right-1 cursor-pointer text-2xl text-accent-100'
        onClick={onClose}
      />
      <Text
        className='row-start-1 row-span-1 col-start-1 col-span-4 self-end'
        as='h4'
        label='confirmationDialog.txt1'
      />
      <Text
        className='row-start-2 row-span-1 col-start-1 col-span-4'
        label={message}
      />
      <Button
        className='row-start-3 row-span-1 col-start-2 col-span-2'
        machine={button.machine}
        text={{
          label: 'confirmationDialog.txt2',
        }}
        data={button.data}
      />
    </div>
  </div>
);

ConfirmationDialog.propTypes = {
  message: propTypes.string.isRequired,
  button: propTypes.shape(Button.propTypes).isRequired,
  onClose: propTypes.func,
};

ConfirmationDialog.defaultProps = {
  onClose: () => {},
};

export default ConfirmationDialog;