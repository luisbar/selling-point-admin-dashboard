import React from 'react';
import Text from '@atoms/Text';
import propTypes from 'prop-types';
import * as icons from 'react-icons/bs';

const TinyCard = ({ title, body, icon, className }) => {
  const renderTitle = () => {
    if (!title) return null;

    return (
      <Text
        className='font-bold'
        label={title.label}
        as={title.as}
      />
    );
  };

  const renderIcon = () => {
    if (!icon.name) return null;
    const Icon = icons[icon.name];

    return (
      <Icon
        className={`text-2xl text-accent-200 ${icon.className}`}
      />
    );
  };

  const renderBody = () => {
    if (!body) return null;

    return (
      <Text
        className={`${icon.name ? 'ml-2' : ''} ${body.className}`}
        label={body.label}
        as={body.as}
      />
    );
  };

  return (
    <div
      className={`rounded-md shadow-md p-3 dark:bg-dark-200 ${className}`}
    >
      {renderTitle()}
      <div
        className='mt-3 flex flex-row items-center'
      >
        {renderIcon()}
        {renderBody()}
      </div>
    </div>
  );
};

TinyCard.propTypes = {
  title: propTypes.shape(Text.propTypes),
  body: propTypes.shape(Text.propTypes),
  icon: propTypes.shape({
    name: propTypes.string.isRequired,
    className: propTypes.string,
  }),
  className: propTypes.string,
};

TinyCard.defaultProps = {
  title: null,
  body: null,
  icon: {
    name: '',
  },
  className: '',
};

export default TinyCard;