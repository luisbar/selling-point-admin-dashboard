import React, { useEffect, useState } from 'react';
import Input from '@atoms/Input';
import Button from '@atoms/Button';
import { useActor } from '@xstate/react';
import propTypes from 'prop-types';

const CreateCategoryForm = ({ machine, className }) => {
  const [ state ] = useActor(machine);
  const [ data, setData ] = useState({
    name: '',
    description: '',
  });

  const onChangeData = ({ target }) => {
    setData((previousDate) => ({
      ...previousDate,
      [target.id]: target.value,
    }));
  };

  useEffect(() => {
    if (state.matches('finished')) setData({
      name: '',
      description: '',
    });
  }, [ state ]);

  return (
    <div
      className={`grid grid-rows-5 grid-cols-1 gap-y-5 ${className}`}
    >
      <Input
        machine={state.children.nameInput}
        className='row-start-1 col-start-1'
        input={{
          type: 'text',
          onChange: onChangeData,
          id: 'name',
          value: data.name,
        }}
        text={{
          label: 'createCategoryForm.txt1',
        }}
      />
      <Input
        machine={state.children.descriptionInput}
        className='row-start-2 col-start-1'
        input={{
          type: 'text',
          onChange: onChangeData,
          id: 'description',
          value: data.description,
        }}
        text={{
          label: 'createCategoryForm.txt2',
        }}
      />
      <Button
        machine={state.children.saveButton}
        className='row-start-3 col-start-1 justify-self-center'
        text={{
          label: 'createCategoryForm.txt3',
        }}
        data={{
          type: 'PROCESS',
          data,
        }}
      />
    </div>
  );
};

CreateCategoryForm.propTypes = {
  machine: propTypes.object.isRequired,
  className: propTypes.string,
};

CreateCategoryForm.defaultProps = {
  className: '',
};

export default CreateCategoryForm;