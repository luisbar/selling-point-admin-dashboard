import React, { useEffect, useState } from 'react';
import Input from '@atoms/Input';
import Button from '@atoms/Button';
import { useActor } from '@xstate/react';
import propTypes from 'prop-types';

const CreateCustomerForm = ({ machine, className }) => {
  const [ state ] = useActor(machine);
  const [ data, setData ] = useState({
    firstName: '',
    lastName: '',
    email: '',
    nit: '',
  });

  const onChangeData = ({ target }) => {
    setData((previousDate) => ({
      ...previousDate,
      [target.id]: target.value,
    }));
  };

  useEffect(() => {
    if (state.matches('finished')) setData({
      firstName: '',
      lastName: '',
      email: '',
      nit: '',
    });
  }, [ state ]);

  return (
    <div
      className={`grid grid-rows-5 grid-cols-2 gap-5 ${className}`}
    >
      <Input
        machine={state.children.firstNameInput}
        className='row-start-1 col-start-1'
        input={{
          type: 'text',
          onChange: onChangeData,
          id: 'firstName',
          value: data.firstName,
        }}
        text={{
          label: 'createCustomerForm.txt1',
        }}
      />
      <Input
        machine={state.children.lastNameInput}
        className='row-start-1 col-start-2'
        input={{
          type: 'text',
          onChange: onChangeData,
          id: 'lastName',
          value: data.lastName,
        }}
        text={{
          label: 'createCustomerForm.txt2',
        }}
      />
      <Input
        machine={state.children.emailInput}
        className='row-start-2 col-start-1'
        input={{
          type: 'text',
          onChange: onChangeData,
          id: 'email',
          value: data.email,
        }}
        text={{
          label: 'createCustomerForm.txt3',
        }}
      />
      <Input
        machine={state.children.nitInput}
        className='row-start-2 col-start-2'
        input={{
          type: 'text',
          onChange: onChangeData,
          id: 'nit',
          value: data.nit,
        }}
        text={{
          label: 'createCustomerForm.txt4',
        }}
      />
      <Button
        machine={state.children.saveButton}
        className='row-start-3 col-start-1 col-span-2 justify-self-center'
        text={{
          label: 'createCustomerForm.txt5',
        }}
        data={{
          type: 'PROCESS',
          data,
        }}
      />
    </div>
  );
};

CreateCustomerForm.propTypes = {
  machine: propTypes.object.isRequired,
  className: propTypes.string,
};

CreateCustomerForm.defaultProps = {
  className: '',
};

export default CreateCustomerForm;