import React, { useState, useEffect } from 'react';
import Input from '@atoms/Input';
import Button from '@atoms/Button';
import Dropdown from '@atoms/Dropdown';
import { useActor } from '@xstate/react';
import propTypes from 'prop-types';

const CreateProductForm = ({ machine, className, enabledCategories }) => {
  const [ state ] = useActor(machine);
  const [ data, setData ] = useState({
    name: '',
    description: '',
    cost: '',
    price: '',
    category: '',
  });

  useEffect(() => {
    if (state.matches('finished')) setData({
      name: '',
      description: '',
      cost: '',
      price: '',
      category: '',
    });
  }, [ state ]);

  useEffect(() => {
    if (enabledCategories.length) setData((currentData) => ({
      ...currentData,
      category: enabledCategories[0].key,
    }));
  }, [ enabledCategories ]);

  const onChangeData = ({ target }) => {
    setData((previousDate) => ({
      ...previousDate,
      [target.id]: target.value,
    }));
  };

  return (
    <div
      className={`grid grid-rows-6 grid-cols-2 gap-5 ${className}`}
    >
      <Input
        machine={state.children.nameInput}
        className='row-start-1 col-start-1'
        input={{
          type: 'text',
          onChange: onChangeData,
          id: 'name',
          value: data.name,
        }}
        text={{
          label: 'createProductForm.txt1',
        }}
      />
      <Input
        machine={state.children.descriptionInput}
        className='row-start-1 col-start-2'
        input={{
          type: 'text',
          onChange: onChangeData,
          id: 'description',
          value: data.description,
        }}
        text={{
          label: 'createProductForm.txt2',
        }}
      />
      <Input
        machine={state.children.costInput}
        className='row-start-2 col-start-1'
        input={{
          type: 'number',
          onChange: onChangeData,
          id: 'cost',
          value: data.cost,
        }}
        text={{
          label: 'createProductForm.txt3',
        }}
      />
      <Input
        machine={state.children.priceInput}
        className='row-start-2 col-start-2'
        input={{
          type: 'number',
          onChange: onChangeData,
          id: 'price',
          value: data.price,
        }}
        text={{
          label: 'createProductForm.txt4',
        }}
      />
      <Dropdown
        machine={state.children.categoryDropdown}
        className='row-start-3 col-start-1 col-span-2'
        text={{
          label: 'createProductForm.txt5',
        }}
        select={{
          id: 'category',
          options: enabledCategories,
          onChange: onChangeData,
          value: data.category,
        }}
      />
      <Button
        machine={state.children.saveButton}
        className='row-start-4 col-start-1 col-span-2 justify-self-center'
        text={{
          label: 'createProductForm.txt6',
        }}
        data={{
          type: 'PROCESS',
          data,
        }}
      />
    </div>
  );
};

CreateProductForm.propTypes = {
  machine: propTypes.object.isRequired,
  className: propTypes.string,
  enabledCategories: propTypes.arrayOf(propTypes.shape({
    key: propTypes.string.isRequired,
    value: propTypes.string.isRequired,
  })).isRequired,
};

CreateProductForm.defaultProps = {
  className: '',
};

export default CreateProductForm;