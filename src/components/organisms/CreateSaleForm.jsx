import React, { useEffect, useState, useRef } from 'react';
import Button from '@atoms/Button';
import Input from '@atoms/Input';
import { useActor } from '@xstate/react';
import propTypes from 'prop-types';
import ProductInputs from '@organisms/ProductInputs';
import Table from '@organisms/Table';
import * as ramda from 'ramda';
import Datalist from '@atoms/Datalist';
import Bill from '@molecules/Bill';
import { useReactToPrint } from 'react-to-print';
import { BsFillPlusSquareFill } from 'react-icons/bs';
import { useNavigation } from 'react-navi';

const CreateSaleForm = ({ machine, className, enabledProducts }) => {
  const [ state ] = useActor(machine);
  const { navigate } = useNavigation();
  const bill = useRef();
  const handlePrint = useReactToPrint({
    content: () => bill.current,
  });
  const [ data, setData ] = useState({
    customerId: '',
    givenMoney: '0',
    change: '0',
    customerName: '',
  });
  const stringifyProducts = JSON.stringify(data.products);

  const onChangeProductQuantity = ({ target }) => {
    setData((currentData) => {
      const { products } = currentData;
      const product = products.filter((productInState) => productInState.id === target.id)[0];
      product.quantity = target.value;
      return {
        ...currentData,
        products,
      };
    });
  };

  const mapProducts = () => {
    if (!Object.keys(enabledProducts).length || !data.products || !data.products.length) return {};

    return ramda.mapObjIndexed((value) => value.map((product, index) => ({
      id: product.id,
      text: {
        label: product.name,
      },
      input: {
        machine: state.context.inputs[index],
        labeless: true,
        input: {
          id: product.id,
          value: data.products
            .filter((productInState) => productInState.id === product.id)[0].quantity,
          type: 'number',
          onChange: onChangeProductQuantity,
        },
      },
    })), enabledProducts);
  };

  const onChangeInputData = ({ target }) => {
    setData((currentData) => ({
      ...currentData,
      [target.id]: target.value,
    }));
  };

  const onChangeDatalistData = ({ id, key, value }) => {
    setData((currentData) => ({
      ...currentData,
      [id]: key,
      customerName: value,
    }));
  };

  useEffect(() => {
    if (state.matches('finished')) setData((currentData) => ({
      customerId: '',
      givenMoney: '0',
      change: '0',
      products: currentData.products.map((product) => ({
        ...product,
        quantity: '0',
      })),
      sale: {
        total: 0,
        selectedProducts: [],
      },
    }));
  }, [ state ]);

  useEffect(() => {
    setData((currentData) => {
      const newData = { ...currentData, products: [] };

      ramda.compose(
        ramda.forEach((product) => { newData.products.push({ id: product.id, quantity: '0', name: product.name, price: product.price }); }),
        ramda.filter((value) => typeof value !== 'string'),
        ramda.flatten,
        ramda.toPairs,
      )(enabledProducts);

      return newData;
    });
  }, [ enabledProducts ]);

  useEffect(() => {
    setData((currentData) => {
      const selectedProducts = currentData.products
        .filter((product) => product.quantity > 0)
        .map((product) => ({
          ...product,
          subtotal: parseFloat(product.quantity) * parseFloat(product.price),
        }));
      const total = selectedProducts
        .reduce((accumulator, product) => accumulator + parseFloat(product.subtotal), 0);

      return {
        ...currentData,
        sale: {
          selectedProducts,
          total,
        },
      };
    });
  }, [ stringifyProducts ]);

  useEffect(() => {
    setData((currentData) => {
      if (currentData.sale) return {
        ...currentData,
        change: String(parseFloat(currentData.givenMoney) - parseFloat(currentData.sale.total)),
      };
      return currentData;
    });
  }, [ data.givenMoney, data.sale ]);

  return (
    <div
      className={`grid grid-rows-5 grid-cols-10 gap-y-5 ${className}`}
    >
      <Bill
        ref={bill}
        data={data}
      />
      <Datalist
        className='row-start-1 col-start-1 col-span-2'
        machine={state.children.customerDatalist}
        text={{
          label: 'createSaleForm.txt1',
        }}
        input={{
          id: 'customerId',
          value: data.customerId,
          onChange: onChangeDatalistData,
        }}
      />
      <BsFillPlusSquareFill
        className='row-start-1 col-start-3 col-span-1 text-3xl text-accent-100 ml-5 mt-7 cursor-pointer'
        onClick={() => navigate('/customer/create/modal')}
      />
      <Input
        className='row-start-1 col-start-4 col-span-2 ml-5'
        machine={state.children.givenMoneyInput}
        input={{
          id: 'givenMoney',
          value: data.givenMoney,
          onChange: onChangeInputData,
          type: 'number',
        }}
        text={{
          label: 'createSaleForm.txt8',
        }}
      />
      <Input
        className='row-start-1 col-start-6 col-span-2 ml-5'
        machine={state.children.changeInput}
        input={{
          id: 'change',
          value: data.change,
          onChange: () => {},
          type: 'number',
        }}
        text={{
          label: 'createSaleForm.txt9',
        }}
      />
      <ProductInputs
        className='row-start-2 row-span-3 col-start-1 col-span-5'
        products={mapProducts()}
      />
      {
        data.sale
        && (
        <Table
          headerActions={[
            {
              icon: 'BsPrinter',
              onClickItem: handlePrint,
              className: 'm-2',
            },
          ]}
          className='row-start-2 row-span-3 col-start-6 col-span-5 print'
          columns={[
            {
              key: 'name',
              value: 'createSaleForm.txt2',
              type: 'text',
            },
            {
              key: 'quantity',
              value: 'createSaleForm.txt3',
              type: 'text',
            },
            {
              key: 'price',
              value: 'createSaleForm.txt4',
              type: 'text',
            },
            {
              key: 'subtotal',
              value: 'createSaleForm.txt5',
              type: 'text',
            },
          ]}
          rows={data.sale.selectedProducts}
        />
        )
      }
      <Input
        className='row-start-5 row-span-1 col-start-10 col-span-1'
        machine={state.children.totalInput}
        input={{
          id: 'total',
          value: data.sale && data.sale.total ? String(data.sale.total) : '0',
          onChange: () => {},
          type: 'number',
        }}
        text={{
          label: 'createSaleForm.txt6',
        }}
      />
      <Button
        machine={state.children.saveButton}
        className='row-start-5 col-start-1 col-span-12 justify-self-center place-self-end'
        text={{
          label: 'createSaleForm.txt7',
        }}
        data={{
          type: 'PROCESS',
          data: {
            customer: data.customerId,
            total: data.sale
              ? data.sale.total
              : 0,
            saleDetail: data.sale
              ? data.sale.selectedProducts.map(({ id, quantity, subtotal }) => ({
                productId: id,
                quantity,
                subtotal,
              }))
              : [],
          },
        }}
      />
    </div>
  );
};

CreateSaleForm.propTypes = {
  machine: propTypes.object.isRequired,
  className: propTypes.string,
  enabledProducts: propTypes.object,
};

CreateSaleForm.defaultProps = {
  className: '',
  enabledProducts: {},
};

export default CreateSaleForm;