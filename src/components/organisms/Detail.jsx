import React from 'react';
import TinyCard from '@root/components/molecules/TinyCard';
import propTypes from 'prop-types';

const Detail = ({ className, items }) => {
  const renderItems = () => items.map((item) => (
    <TinyCard
      key={(item.title && item.title.label) || (item.body && item.body.label)}
      className={item.className}
      title={item.title}
      body={item.body}
      icon={item.icon}
    />
  ));

  return (
    <div
      className={`grid grid-rows-6 grid-cols-10 gap-x-5 items-center ${className}`}
    >
      {renderItems()}
    </div>
  );
};

Detail.propTypes = {
  className: propTypes.string,
  items: propTypes.arrayOf(propTypes.shape(TinyCard.propTypes).isRequired).isRequired,
};

Detail.defaultProps = {
  className: '',
};

export default Detail;