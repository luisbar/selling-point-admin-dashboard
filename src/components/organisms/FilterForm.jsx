import React, { useState } from 'react';
import Dropdown from '@atoms/Dropdown';
import Input from '@atoms/Input';
import Button from '@atoms/Button';
import { useActor } from '@xstate/react';
import propTypes from 'prop-types';

const FilterForm = ({ machine, defaultPageSize, defaultFilter, className }) => {
  const [ state ] = useActor(machine);
  const [ data, setData ] = useState({
    pageSize: defaultPageSize,
    filter: defaultFilter,
  });

  const onChangeData = ({ target }) => {
    setData((currentData) => ({
      ...currentData,
      [target.id]: target.value,
    }));
  };

  return (
    <div
      className={`grid grid-cols-3 grid-rows-1 gap-4 ${className}`}
    >
      <Dropdown
        machine={state.children.pageSizeDropdown}
        className='row-start-1 col-start-1 self-end'
        text={{
          label: 'filterForm.txt1',
        }}
        select={{
          id: 'pageSize',
          onChange: onChangeData,
          options: [
            {
              key: 'filterForm.txt4',
              value: 'filterForm.txt4',
            },
            {
              key: 'filterForm.txt5',
              value: 'filterForm.txt5',
            },
            {
              key: 'filterForm.txt6',
              value: 'filterForm.txt6',
            },
          ],
          value: data.pageSize,
        }}
      />
      <Input
        machine={state.children.filterInput}
        className='row-start-1 col-start-2 self-end'
        input={{
          type: 'text',
          onChange: onChangeData,
          value: data.filter,
          id: 'filter',
        }}
        text={{
          label: 'filterForm.txt2',
        }}
      />
      <Button
        machine={state.children.filterButton}
        className='row-start-1 col-start-3 self-end'
        text={{
          label: 'filterForm.txt3',
        }}
        data={{
          type: 'PROCESS',
          data: {
            ...data,
            pageNumber: 0,
          },
        }}
      />
    </div>
  );
};

FilterForm.propTypes = {
  machine: propTypes.object.isRequired,
  defaultPageSize: propTypes.string.isRequired,
  defaultFilter: propTypes.string,
  className: propTypes.string,
};

FilterForm.defaultProps = {
  defaultFilter: '',
  className: '',
};

export default FilterForm;