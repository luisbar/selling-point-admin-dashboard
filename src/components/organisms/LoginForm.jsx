import React, { useState } from 'react';
import Text from '@atoms/Text';
import Input from '@atoms/Input';
import Button from '@atoms/Button';
import { useStoreContext } from '@stateMachines/Store';
import { useActor } from '@xstate/react';

const LoginForm = () => {
  const { loginService } = useStoreContext();
  const [ state ] = useActor(loginService.children.get('loginForm'));
  const [ credentials, setCredentials ] = useState({
    username: '',
    password: '',
  });

  const onChange = (event) => {
    setCredentials((currentCredentials) => ({
      ...currentCredentials,
      [event.target.id]: event.target.value,
    }));
  };

  return (
    <div
      className='flex flex-col items-center'
    >
      <Text
        as='h3'
        className='text-white'
        label='loginForm.txt1'
      />
      <Input
        machine={state.children.usernameInput}
        className='mt-6'
        text={{
          label: 'loginForm.txt2',
          className: 'text-white',
        }}
        input={{
          onChange,
          type: 'email',
          id: 'username',
          value: credentials.username,
        }}
      />
      <Input
        machine={state.children.passwordInput}
        className='mt-6'
        text={{
          label: 'loginForm.txt3',
          className: 'text-white',
        }}
        input={{
          onChange,
          type: 'password',
          id: 'password',
          value: credentials.password,
        }}
      />
      <Button
        machine={state.children.loginButton}
        className='mt-6'
        data={{
          type: 'PROCESS',
          data: {
            ...credentials,
          },
        }}
        text={{
          label: 'loginForm.txt4',
          className: 'text-white',
        }}
      />
    </div>
  );
};

export default LoginForm;