import React from 'react';
import Input from '@atoms/Input';
import propTypes from 'prop-types';
import Text from '@atoms/Text';

const ProductInputs = ({ className, products }) => {
  const renderProducts = ({ category }) => products[category]
    .map(({ id, input: { machine, ...others }, text }) => (
      <Input
        machine={machine}
        key={id}
        text={text}
        input={others.input}
      />
    ));

  const renderProductsCategorized = () => Object.keys(products).map((category) => (
    <div
      key={category}
      className='flex flex-col gap-2'
    >
      <Text
        as='h4'
        label={category}
      />
      <div
        className='flex flex-row flex-wrap gap-2'
      >
        {renderProducts({ category, products })}
      </div>
    </div>
  ));

  return (
    <div
      className={`flex flex-col gap-5 overflow-y-scroll ${className}`}
    >
      {renderProductsCategorized()}
    </div>
  );
};

ProductInputs.propTypes = {
  className: propTypes.string,
  products: propTypes.object.isRequired,
};

ProductInputs.defaultProps = {
  className: '',
};

export default ProductInputs;