import React from 'react';
import LogOutForm from '@organisms/LogOutForm';
import MenuItem from '@atoms/MenuItem';
import propTypes from 'prop-types';
import Text from '@atoms/Text';

const SideMenu = ({ items }) => {
  const renderItems = () => items.map(({ path, value }, index) => (
    <MenuItem
      key={value}
      path={path}
      label={value}
      className={`row-start-${index + 2} row-end-${index + 2}`}
    />
  ));

  return (
    <div
      className='w-40 h-full shadow-md bg-primary dark:bg-dark-200 grid grid-rows-side-menu'
    >
      <LogOutForm />
      {renderItems()}
      <Text
        className='row-start-12 text-light-100 justify-self-center self-end'
        label={process.env.PACKAGE_VERSION}
      />
    </div>
  );
};

SideMenu.propTypes = {
  items: propTypes.arrayOf(
    propTypes.shape({
      path: propTypes.string.isRequired,
      value: propTypes.string.isRequired,
    }).isRequired,
  ).isRequired,
};

export default SideMenu;