import React from 'react';
import propTypes from 'prop-types';
import TableCell from '@atoms/TableCell';

const Table = ({ columns, rows, className, headerActions }) => {
  const renderHeaderActions = () => (
    <TableCell
      type='headerActions'
      headerActions={headerActions}
      columnsCount={columns.length + 1}
    />
  );

  const renderColums = () => columns.map(({ value }, columnIndex) => (
    <TableCell
      key={`column-${value}`}
      type='header'
      label={value}
      columnIndex={columnIndex}
    />
  ));

  const renderRows = () => (
    <div
      className={`grid grid-rows-table-body-${rows.length} grid-cols-${columns.length} row-start-3 col-start-1 col-end-${columns.length + 1} overflow-y-scroll`}
    >
      {
          rows.map((row, rowIndex) => columns.map(({ key, type, actions, format }, columnIndex) => (
            <TableCell
              key={`cell-${row.id}-${key}`}
              type={type}
              row={row}
              rowIndex={rowIndex}
              columnKey={key}
              actions={actions}
              columnIndex={columnIndex}
              format={format}
            />
          )))
        }
    </div>
  );

  return (
    <div className={`shadow-md grid grid-rows-table grid-cols-${columns.length} ${className} dark:bg-dark-200`}>
      {renderHeaderActions()}
      {renderColums()}
      {renderRows()}
    </div>
  );
};

Table.propTypes = {
  columns: propTypes.arrayOf(
    propTypes.shape({
      key: propTypes.string.isRequired,
      value: propTypes.string.isRequired,
      type: propTypes.oneOf([ 'text', 'actions', 'boolean', 'date' ]).isRequired,
      actions: propTypes.arrayOf(
        propTypes.shape({
          icon: propTypes.string.isRequired,
          onClickItem: propTypes.func.isRequired,
        }).isRequired,
      ),
    }).isRequired,
  ).isRequired,
  rows: propTypes.arrayOf(propTypes.object).isRequired,
  className: propTypes.string,
  headerActions: propTypes.arrayOf(
    propTypes.shape({
      icon: propTypes.string.isRequired,
      onClickItem: propTypes.func.isRequired,
      className: propTypes.string,
    }).isRequired,
  ),
};

Table.defaultProps = {
  className: '',
  headerActions: [],
};

export default Table;