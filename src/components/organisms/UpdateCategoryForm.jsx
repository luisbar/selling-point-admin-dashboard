import React, { useState, useEffect } from 'react';
import Input from '@atoms/Input';
import Button from '@atoms/Button';
import Checkbox from '@atoms/Checkbox';
import { useActor } from '@xstate/react';
import propTypes from 'prop-types';
import { useNavigation } from 'react-navi';

const UpdateCategoryForm = ({ machine, className, defaultValues }) => {
  const [ state ] = useActor(machine);
  const { navigate } = useNavigation();
  const [ data, setData ] = useState({
    name: defaultValues.name,
    description: defaultValues.description,
    enabled: defaultValues.enabled,
  });

  const onChangeData = ({ target }) => {
    setData((previousDate) => ({
      ...previousDate,
      [target.id]: target.value,
    }));
  };

  useEffect(() => {
    if (state.matches('finished')) navigate('/categories');
  }, [ state ]);

  return (
    <div
      className={`grid grid-rows-5 grid-cols-1 gap-y-5 ${className}`}
    >
      <Input
        machine={state.children.nameInput}
        className='row-start-1 col-start-1'
        input={{
          type: 'text',
          onChange: onChangeData,
          id: 'name',
          value: data.name,
        }}
        text={{
          label: 'updateCategoryForm.txt1',
        }}
      />
      <Input
        machine={state.children.descriptionInput}
        className='row-start-2 col-start-1'
        input={{
          type: 'text',
          onChange: onChangeData,
          id: 'description',
          value: data.description,
        }}
        text={{
          label: 'updateCategoryForm.txt2',
        }}
      />
      <Checkbox
        id='enabled'
        className='row-start-3 col-start-1'
        value={data.enabled}
        onChange={onChangeData}
        label='updateCategoryForm.txt3'
        machine={state.children.enabledCheckbox}
        data={{
          type: '',
        }}
      />
      <Button
        machine={state.children.saveButton}
        className='row-start-4 col-start-1 justify-self-center'
        text={{
          label: 'updateCategoryForm.txt4',
        }}
        data={{
          type: 'PROCESS',
          data: {
            ...data,
            id: defaultValues.id,
          },
        }}
      />
    </div>
  );
};

UpdateCategoryForm.propTypes = {
  machine: propTypes.object.isRequired,
  className: propTypes.string,
  defaultValues: propTypes.shape({
    id: propTypes.string.isRequired,
    name: propTypes.string.isRequired,
    description: propTypes.string.isRequired,
    enabled: propTypes.bool.isRequired,
  })
    .isRequired,
};

UpdateCategoryForm.defaultProps = {
  className: '',
};

export default UpdateCategoryForm;