import React, { useState, useEffect } from 'react';
import Input from '@atoms/Input';
import Button from '@atoms/Button';
import Checkbox from '@atoms/Checkbox';
import { useActor } from '@xstate/react';
import propTypes from 'prop-types';
import { useNavigation } from 'react-navi';

const UpdateCustomerForm = ({ machine, className, defaultValues }) => {
  const [ state ] = useActor(machine);
  const { navigate } = useNavigation();
  const [ data, setData ] = useState({
    firstName: defaultValues.firstName,
    lastName: defaultValues.lastName,
    email: defaultValues.email,
    nit: defaultValues.nit,
    enabled: defaultValues.enabled,
  });

  const onChangeData = ({ target }) => {
    setData((previousDate) => ({
      ...previousDate,
      [target.id]: target.value,
    }));
  };

  useEffect(() => {
    if (state.matches('finished')) navigate('/customers');
  }, [ state ]);

  return (
    <div
      className={`grid grid-rows-5 grid-cols-2 gap-5 ${className}`}
    >
      <Input
        machine={state.children.firstNameInput}
        className='row-start-1 col-start-1'
        input={{
          type: 'text',
          onChange: onChangeData,
          id: 'firstName',
          value: data.firstName,
        }}
        text={{
          label: 'updateCustomerForm.txt1',
        }}
      />
      <Input
        machine={state.children.lastNameInput}
        className='row-start-1 col-start-2'
        input={{
          type: 'text',
          onChange: onChangeData,
          id: 'lastName',
          value: data.lastName,
        }}
        text={{
          label: 'updateCustomerForm.txt2',
        }}
      />
      <Input
        machine={state.children.emailInput}
        className='row-start-2 col-start-1'
        input={{
          type: 'text',
          onChange: onChangeData,
          id: 'email',
          value: data.email,
        }}
        text={{
          label: 'updateCustomerForm.txt3',
        }}
      />
      <Input
        machine={state.children.nitInput}
        className='row-start-2 col-start-2'
        input={{
          type: 'text',
          onChange: onChangeData,
          id: 'nit',
          value: data.nit,
        }}
        text={{
          label: 'updateCustomerForm.txt4',
        }}
      />
      <Checkbox
        id='enabled'
        className='row-start-3 col-start-1 col-span-2'
        value={data.enabled}
        onChange={onChangeData}
        label='updateCustomerForm.txt5'
        machine={state.children.enabledCheckbox}
        data={{
          type: '',
        }}
      />
      <Button
        machine={state.children.saveButton}
        className='row-start-4 col-start-1 col-span-2 justify-self-center'
        text={{
          label: 'updateCustomerForm.txt6',
        }}
        data={{
          type: 'PROCESS',
          data: {
            ...data,
            id: defaultValues.id,
          },
        }}
      />
    </div>
  );
};

UpdateCustomerForm.propTypes = {
  machine: propTypes.object.isRequired,
  className: propTypes.string,
  defaultValues: propTypes.shape({
    id: propTypes.string.isRequired,
    firstName: propTypes.string.isRequired,
    lastName: propTypes.string.isRequired,
    email: propTypes.string.isRequired,
    nit: propTypes.string.isRequired,
    enabled: propTypes.bool.isRequired,
  })
    .isRequired,
};

UpdateCustomerForm.defaultProps = {
  className: '',
};

export default UpdateCustomerForm;