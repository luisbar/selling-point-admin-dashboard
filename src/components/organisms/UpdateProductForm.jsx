import React, { useState, useEffect } from 'react';
import Input from '@atoms/Input';
import Button from '@atoms/Button';
import Dropdown from '@atoms/Dropdown';
import Checkbox from '@atoms/Checkbox';
import { useActor } from '@xstate/react';
import propTypes from 'prop-types';
import { useNavigation } from 'react-navi';

const UpdateProductForm = ({ machine, className, enabledCategories, defaultValues }) => {
  const [ state ] = useActor(machine);
  const { navigate } = useNavigation();
  const [ data, setData ] = useState({
    name: defaultValues.name,
    description: defaultValues.description,
    cost: String(defaultValues.cost),
    price: String(defaultValues.price),
    category: defaultValues.category.id,
    enabled: defaultValues.enabled,
  });

  const onChangeData = ({ target }) => {
    setData((previousDate) => ({
      ...previousDate,
      [target.id]: target.value,
    }));
  };

  useEffect(() => {
    if (state.matches('finished')) navigate('/products');
  }, [ state ]);

  return (
    <div
      className={`grid grid-rows-6 grid-cols-2 gap-5 ${className}`}
    >
      <Input
        machine={state.children.nameInput}
        className='row-start-1 col-start-1'
        input={{
          type: 'text',
          onChange: onChangeData,
          id: 'name',
          value: data.name,
        }}
        text={{
          label: 'updateProductForm.txt1',
        }}
      />
      <Input
        machine={state.children.descriptionInput}
        className='row-start-1 col-start-2'
        input={{
          type: 'text',
          onChange: onChangeData,
          id: 'description',
          value: data.description,
        }}
        text={{
          label: 'updateProductForm.txt2',
        }}
      />
      <Input
        machine={state.children.costInput}
        className='row-start-2 col-start-1'
        input={{
          type: 'number',
          onChange: onChangeData,
          id: 'cost',
          value: data.cost,
        }}
        text={{
          label: 'updateProductForm.txt3',
        }}
      />
      <Input
        machine={state.children.priceInput}
        className='row-start-2 col-start-2'
        input={{
          type: 'number',
          onChange: onChangeData,
          id: 'price',
          value: data.price,
        }}
        text={{
          label: 'updateProductForm.txt4',
        }}
      />
      <Dropdown
        machine={state.children.categoryDropdown}
        className='row-start-3 col-start-1'
        text={{
          label: 'updateProductForm.txt5',
        }}
        select={{
          id: 'category',
          options: enabledCategories,
          onChange: onChangeData,
          value: data.category,
        }}
      />

      <Checkbox
        id='enabled'
        className='row-start-3 col-start-2'
        value={data.enabled}
        onChange={onChangeData}
        label='updateProductForm.txt6'
        machine={state.children.enabledCheckbox}
        data={{
          type: '',
        }}
      />
      <Button
        machine={state.children.saveButton}
        className='row-start-4 col-start-1 col-span-2 justify-self-center'
        text={{
          label: 'updateProductForm.txt7',
        }}
        data={{
          type: 'PROCESS',
          data: {
            ...data,
            id: defaultValues.id,
          },
        }}
      />
    </div>
  );
};

UpdateProductForm.propTypes = {
  machine: propTypes.object.isRequired,
  className: propTypes.string,
  enabledCategories: propTypes.arrayOf(propTypes.shape({
    key: propTypes.string.isRequired,
    value: propTypes.string.isRequired,
  })).isRequired,
  defaultValues: propTypes.shape({
    id: propTypes.string.isRequired,
    name: propTypes.string.isRequired,
    description: propTypes.string.isRequired,
    cost: propTypes.number.isRequired,
    price: propTypes.number.isRequired,
    category: propTypes.shape({
      id: propTypes.string.isRequired,
    }).isRequired,
    enabled: propTypes.bool.isRequired,
  }).isRequired,
};

UpdateProductForm.defaultProps = {
  className: '',
};

export default UpdateProductForm;