import React, { useEffect } from 'react';
import Main from '@templates/Main';
import Table from '@organisms/Table';
import { useGlobalSpinnerContext } from '@atoms/GlobalSpinner';
import { useGlobalAlertContext } from '@molecules/GlobalAlert';
import Paginator from '@molecules/Paginator';
import FilterForm from '@organisms/FilterForm';
import { useStoreContext } from '@stateMachines/Store';
import { useActor } from '@xstate/react';
import { useNavigation } from 'react-navi';

const Categories = () => {
  const { categoriesService } = useStoreContext();
  const [ state, send ] = useActor(categoriesService);
  const toggleSpinner = [ ...useGlobalSpinnerContext() ].pop();
  const toggleAlert = [ ...useGlobalAlertContext() ].pop();
  const { navigate } = useNavigation();

  const onViewItem = (data) => {
    navigate(`/category/view/${data.id}`, { body: data });
  };

  const onUpdateItem = (data) => {
    navigate(`/category/update/${data.id}`, { body: data });
  };

  useEffect(() => {
    if (state.matches('processing')) toggleSpinner({ type: 'ENABLE' });
    if (!state.matches('processing')) toggleSpinner({ type: 'DISABLE' });
    if (state.matches('failed')) toggleAlert({ type: 'ENABLE', data: { type: 'error', message: state.context.error.message } });
  }, [ state.value ]);

  useEffect(() => {
    send({
      type: 'PROCESS',
      data: {
        pageNumber: state.context.pagination.pageNumber,
        pageSize: state.context.pagination.pageSize,
      },
    });
  }, []);

  return (
    <Main>
      <FilterForm
        className='row-start-1 row-span-1 col-start-1 col-span-4'
        machine={state.children.filterForm}
        defaultPageSize={state.context.pagination.pageSize}
        defaultFilter={state.context.filter}
      />
      <Table
        headerActions={[
          {
            icon: 'BsFillPlusSquareFill',
            onClickItem: () => navigate('/category/create'),
          },
        ]}
        className='row-start-2 row-span-1 col-start-1 col-span-10'
        columns={[
          { key: 'id', value: 'categories.txt1', type: 'text' },
          { key: 'name', value: 'categories.txt2', type: 'text' },
          { key: 'enabled', value: 'categories.txt3', type: 'boolean' },
          { key: 'createdAt', value: 'categories.txt4', type: 'date', format: 'DD/MM/YYYY HH:mm:ss' },
          { key: 'actions',
            value: 'categories.txt5',
            type: 'actions',
            actions: [
              { icon: 'BsEye', onClickItem: onViewItem },
              { icon: 'BsPencil', onClickItem: onUpdateItem },
            ] },
        ]}
        rows={state.context.categories}
        rowsCount={parseInt(state.context.pagination.pageSize, 10)}
      />
      <Paginator
        machine={state.children.paginator}
        className='row-start-3 row-span-1 col-start-7 col-span-4 h-14'
      />
    </Main>
  );
};

export default Categories;
