import CreateCategoryForm from '@organisms/CreateCategoryForm';
import { useGlobalSpinnerContext } from '@atoms/GlobalSpinner';
import { useStoreContext } from '@stateMachines/Store';
import Main from '@templates/Main';
import { useActor } from '@xstate/react';
import React, { useEffect } from 'react';
import { useGlobalAlertContext } from '@molecules/GlobalAlert';

const CreateCategory = () => {
  const { createCategoryService } = useStoreContext();
  const [ createCategoryFormState ] = useActor(createCategoryService.children.get('createCategoryForm'));
  const toggleSpinner = [ ...useGlobalSpinnerContext() ].pop();
  const toggleAlert = [ ...useGlobalAlertContext() ].pop();

  useEffect(() => {
    if (createCategoryFormState.matches('processing')) toggleSpinner({ type: 'ENABLE' });
    if (!createCategoryFormState.matches('processing')) toggleSpinner({ type: 'DISABLE' });
    if (createCategoryFormState.matches('failed')) toggleAlert({
      type: 'ENABLE',
      data: { type: 'error', message: createCategoryFormState.context.error.message },
    });
    if (createCategoryFormState.matches('finished')) toggleAlert({
      type: 'ENABLE',
      data: { type: 'success', message: 'createCategory.txt1' },
    });
  }, [ createCategoryFormState.value ]);

  return (
    <Main>
      <CreateCategoryForm
        className='row-start-2 row-span-1 col-start-4 col-span-4 self-center'
        machine={createCategoryService.children.get('createCategoryForm')}
      />
    </Main>
  );
};

export default CreateCategory;