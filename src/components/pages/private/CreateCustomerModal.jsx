import CreateCustomerForm from '@organisms/CreateCustomerForm';
import { useGlobalSpinnerContext } from '@atoms/GlobalSpinner';
import { useStoreContext } from '@stateMachines/Store';
import Modal from '@templates/Modal';
import { useActor } from '@xstate/react';
import React, { useEffect } from 'react';
import { useGlobalAlertContext } from '@molecules/GlobalAlert';

const CreateCustomerModal = () => {
  const { createCustomerService } = useStoreContext();
  const [ createCustomerFormState ] = useActor(createCustomerService.children.get('createCustomerForm'));
  const toggleSpinner = [ ...useGlobalSpinnerContext() ].pop();
  const toggleAlert = [ ...useGlobalAlertContext() ].pop();

  useEffect(() => {
    if (createCustomerFormState.matches('processing')) toggleSpinner({ type: 'ENABLE' });
    if (!createCustomerFormState.matches('processing')) toggleSpinner({ type: 'DISABLE' });
    if (createCustomerFormState.matches('failed')) toggleAlert({
      type: 'ENABLE',
      data: { type: 'error', message: createCustomerFormState.context.error.message },
    });
    if (createCustomerFormState.matches('finished')) toggleAlert({
      type: 'ENABLE',
      data: { type: 'success', message: 'createCustomer.txt1' },
    });
  }, [ createCustomerFormState.value ]);

  return (
    <Modal>
      <CreateCustomerForm
        className=''
        machine={createCustomerService.children.get('createCustomerForm')}
      />
    </Modal>
  );
};

export default CreateCustomerModal;