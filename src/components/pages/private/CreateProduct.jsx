import CreateProductForm from '@organisms/CreateProductForm';
import { useGlobalSpinnerContext } from '@atoms/GlobalSpinner';
import { useStoreContext } from '@stateMachines/Store';
import Main from '@templates/Main';
import { useActor } from '@xstate/react';
import React, { useEffect } from 'react';
import { useGlobalAlertContext } from '@molecules/GlobalAlert';

const CreateProduct = () => {
  const { createProductService } = useStoreContext();
  const [ state, send ] = useActor(createProductService);
  const [ createProductFormState ] = useActor(createProductService.children.get('createProductForm'));
  const toggleSpinner = [ ...useGlobalSpinnerContext() ].pop();
  const toggleAlert = [ ...useGlobalAlertContext() ].pop();

  useEffect(() => {
    send({
      type: 'PROCESS',
    });
  }, []);

  useEffect(() => {
    if (createProductFormState.matches('processing')) toggleSpinner({ type: 'ENABLE' });
    if (!createProductFormState.matches('processing')) toggleSpinner({ type: 'DISABLE' });
    if (createProductFormState.matches('failed')) toggleAlert({
      type: 'ENABLE',
      data: { type: 'error', message: createProductFormState.context.error.message },
    });
    if (createProductFormState.matches('finished')) toggleAlert({
      type: 'ENABLE',
      data: { type: 'success', message: 'createProduct.txt1' },
    });
  }, [ createProductFormState.value ]);

  return (
    <Main>
      <CreateProductForm
        enabledCategories={state.context.enabledCategories}
        className='row-start-2 row-span-1 col-start-4 col-span-4 self-center'
        machine={createProductService.children.get('createProductForm')}
      />
    </Main>
  );
};

export default CreateProduct;