import CreateSaleForm from '@organisms/CreateSaleForm';
import { useGlobalSpinnerContext } from '@atoms/GlobalSpinner';
import { useStoreContext } from '@stateMachines/Store';
import Main from '@templates/Main';
import { useActor } from '@xstate/react';
import React, { useEffect } from 'react';
import { useGlobalAlertContext } from '@molecules/GlobalAlert';

const CreateSale = () => {
  const { createSaleService } = useStoreContext();
  const [ state, send ] = useActor(createSaleService);
  const [ createSaleFormState ] = useActor(createSaleService.children.get('createSaleForm'));
  const toggleSpinner = [ ...useGlobalSpinnerContext() ].pop();
  const toggleAlert = [ ...useGlobalAlertContext() ].pop();

  useEffect(() => {
    send({
      type: 'PROCESS',
    });
  }, []);

  useEffect(() => {
    if (createSaleFormState.matches('processing')) toggleSpinner({ type: 'ENABLE' });
    if (!createSaleFormState.matches('processing')) toggleSpinner({ type: 'DISABLE' });
    if (createSaleFormState.matches('failed')) toggleAlert({
      type: 'ENABLE',
      data: { type: 'error', message: createSaleFormState.context.error.message },
    });
    if (createSaleFormState.matches('finished')) toggleAlert({
      type: 'ENABLE',
      data: { type: 'success', message: 'createSale.txt1' },
    });
  }, [ createSaleFormState.value ]);

  return (
    <Main>
      <CreateSaleForm
        enabledCustomers={state.context.enabledCustomers}
        enabledProducts={state.context.enabledProducts}
        className='row-start-2 row-span-1 col-start-1 col-span-10'
        machine={createSaleService.children.get('createSaleForm')}
      />
    </Main>
  );
};

export default CreateSale;