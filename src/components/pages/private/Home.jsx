import React, { useEffect } from 'react';
import Main from '@templates/Main';
import { useGlobalSpinnerContext } from '@atoms/GlobalSpinner';
import { useStoreContext } from '@root/stateMachines/Store';
import { useActor } from '@xstate/react';
import { useGlobalAlertContext } from '@molecules/GlobalAlert';
import TinyCard from '@molecules/TinyCard';
import Chart from '@molecules/Chart';

const Home = () => {
  const { homeService } = useStoreContext();
  const [ state, send ] = useActor(homeService);
  const globalSpinnerContext = useGlobalSpinnerContext();
  const toggleSpinner = [ ...globalSpinnerContext ].pop();
  const toggleAlert = [ ...useGlobalAlertContext() ].pop();

  useEffect(() => {
    send({
      type: 'PROCESS',
    });
  }, []);

  useEffect(() => {
    if (state.matches('processing')) toggleSpinner({ type: 'ENABLE' });
    if (state.matches('finished')) toggleSpinner({ type: 'DISABLE' });
    if (state.matches('failed')) toggleAlert({ type: 'ENABLE', data: { type: 'error', message: state.context.error.message } });
  }, [ state.value ]);

  return (
    <Main>
      <div className='row-start-2 row-span-1 col-start-1 col-span-10 grid grid-rows-6 grid-cols-10 gap-5'>
        <TinyCard
          className='row-start-1 row-span-2 col-start-1 col-span-2 my-auto'
          title={{
            label: 'home.txt1',
          }}
          body={{
            label: state.context.monthlyRevenue,
            as: 'h5',
          }}
          icon={{
            name: 'BsCurrencyDollar',
            className: 'text-success',
          }}
        />
        <TinyCard
          className='row-start-1 row-span-2 col-start-3 col-span-2 my-auto'
          title={{
            label: 'home.txt2',
          }}
          body={{
            label: state.context.weeklyRevenue,
            as: 'h5',
          }}
          icon={{
            name: 'BsCurrencyDollar',
            className: 'text-success',
          }}
        />
        <TinyCard
          className='row-start-1 row-span-2 col-start-5 col-span-2 my-auto'
          title={{
            label: 'home.txt3',
          }}
          body={{
            label: state.context.dailyRevenue,
            as: 'h5',
          }}
          icon={{
            name: 'BsCurrencyDollar',
            className: 'text-success',
          }}
        />
        <Chart
          className='row-start-3 row-span-4 col-start-1 col-span-5'
          machine={state.children.salesByStatusChart}
          type='Pie'
          period='month'
          text={{
            as: 'h5',
            label: 'home.txt4',
          }}
        />
        <Chart
          className='row-start-3 row-span-4 col-start-6 col-span-5'
          machine={state.children.saleAmountPerDayChart}
          type='Line'
          period='day'
          text={{
            as: 'h5',
            label: 'home.txt5',
          }}
        />
      </div>
    </Main>
  );
};

export default Home;
