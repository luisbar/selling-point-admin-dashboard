import React, { useEffect, useState } from 'react';
import Main from '@templates/Main';
import Table from '@organisms/Table';
import { useGlobalSpinnerContext } from '@atoms/GlobalSpinner';
import { useGlobalAlertContext } from '@molecules/GlobalAlert';
import Paginator from '@molecules/Paginator';
import FilterForm from '@organisms/FilterForm';
import { useStoreContext } from '@stateMachines/Store';
import { useActor } from '@xstate/react';
import { useNavigation } from 'react-navi';
import ConfirmationDialog from '@root/components/molecules/ConfirmationDialog';

const Sales = () => {
  const { salesService } = useStoreContext();
  const [ state, send ] = useActor(salesService);
  const [ cancelationSaleDialogState ] = useActor(state.children.saleCancelationDialog);
  const [ completionSaleDialogState ] = useActor(state.children.saleCompletionDialog);
  const toggleSpinner = [ ...useGlobalSpinnerContext() ].pop();
  const toggleAlert = [ ...useGlobalAlertContext() ].pop();
  const { navigate } = useNavigation();
  const [ showCancelationDialog, setShowCancelationDialog ] = useState(null);
  const [ showCompletionDialog, setShowCompletionDialog ] = useState(null);

  const onViewItem = (data) => {
    navigate(`/sale/view/${data.id}`, { body: data });
  };

  const onUpdateItem = (data) => {
    navigate(`/sale/update/${data.id}`, { body: data });
  };

  const onCancelSale = (data) => {
    setShowCancelationDialog(() => data);
  };

  const onCompleteSale = (data) => {
    setShowCompletionDialog(() => data);
  };

  const onCloseConfirmationDialog = (setState) => () => {
    setState(() => null);
  };

  useEffect(() => {
    if (state.matches('processing')) toggleSpinner({ type: 'ENABLE' });
    if (!state.matches('processing')) toggleSpinner({ type: 'DISABLE' });
    if (state.matches('failed')) toggleAlert({ type: 'ENABLE', data: { type: 'error', message: state.context.error.message } });
  }, [ state.value ]);

  useEffect(() => {
    if (cancelationSaleDialogState.matches('processing')) toggleSpinner({ type: 'ENABLE' });
    if (!cancelationSaleDialogState.matches('processing')) toggleSpinner({ type: 'DISABLE' });
    if (cancelationSaleDialogState.matches('failed')) toggleAlert({ type: 'ENABLE', data: { type: 'error', message: cancelationSaleDialogState.context.error.message } });
    if (cancelationSaleDialogState.matches('finished')) setShowCancelationDialog(() => null);
    if (cancelationSaleDialogState.matches('finished')) toggleAlert({
      type: 'ENABLE',
      data: { type: 'success', message: 'sales.txt9' },
    });
  }, [ cancelationSaleDialogState.value ]);

  useEffect(() => {
    if (completionSaleDialogState.matches('processing')) toggleSpinner({ type: 'ENABLE' });
    if (!completionSaleDialogState.matches('processing')) toggleSpinner({ type: 'DISABLE' });
    if (completionSaleDialogState.matches('failed')) toggleAlert({ type: 'ENABLE', data: { type: 'error', message: completionSaleDialogState.context.error.message } });
    if (completionSaleDialogState.matches('finished')) setShowCompletionDialog(() => null);
    if (completionSaleDialogState.matches('finished')) toggleAlert({
      type: 'ENABLE',
      data: { type: 'success', message: 'sales.txt10' },
    });
  }, [ completionSaleDialogState.value ]);

  useEffect(() => {
    send({
      type: 'PROCESS',
      data: {
        pageNumber: state.context.pagination.pageNumber,
        pageSize: state.context.pagination.pageSize,
      },
    });
  }, []);

  return (
    <Main>
      <FilterForm
        className='row-start-1 row-span-1 col-start-1 col-span-4'
        machine={state.children.filterForm}
        defaultPageSize={state.context.pagination.pageSize}
        defaultFilter={state.context.filter}
      />
      <Table
        headerActions={[
          {
            icon: 'BsFillPlusSquareFill',
            onClickItem: () => navigate('/sale/create'),
          },
        ]}
        className='row-start-2 row-span-1 col-start-1 col-span-10'
        columns={[
          { key: 'id', value: 'sales.txt1', type: 'text' },
          { key: 'customerName', value: 'sales.txt2', type: 'text' },
          { key: 'total', value: 'sales.txt3', type: 'text' },
          { key: 'status', value: 'sales.txt4', type: 'text' },
          { key: 'createdAt', value: 'sales.txt5', type: 'date', format: 'DD/MM/YYYY HH:mm:ss' },
          { key: 'actions',
            value: 'sales.txt6',
            type: 'actions',
            actions: [
              { icon: 'BsEye', onClickItem: onViewItem },
              { icon: 'BsPencil', onClickItem: onUpdateItem },
              { icon: 'BsFillCartXFill', onClickItem: onCancelSale, className: 'text-error' },
              { icon: 'BsFillCartCheckFill', onClickItem: onCompleteSale, className: 'text-success' },
            ] },
        ]}
        rows={state.context.sales}
        rowsCount={parseInt(state.context.pagination.pageSize, 10)}
      />
      {
        showCancelationDialog
        && (
        <ConfirmationDialog
          message='sales.txt7'
          onClose={onCloseConfirmationDialog(setShowCancelationDialog)}
          button={{
            machine: state.children.saleCancelationDialog.children.get('confirmationButton'),
            data: {
              type: 'PROCESS',
              data: showCancelationDialog.id,
            },
            text: {
              label: '',
            },
          }}
        />
        )
      }
      {
        showCompletionDialog
        && (
        <ConfirmationDialog
          message='sales.txt8'
          onClose={onCloseConfirmationDialog(setShowCompletionDialog)}
          button={{
            machine: state.children.saleCompletionDialog.children.get('confirmationButton'),
            data: {
              type: 'PROCESS',
              data: showCompletionDialog.id,
            },
            text: {
              label: '',
            },
          }}
        />
        )
      }
      <Paginator
        machine={state.children.paginator}
        className='row-start-3 row-span-1 col-start-7 col-span-4 h-14'
      />
    </Main>
  );
};

export default Sales;
