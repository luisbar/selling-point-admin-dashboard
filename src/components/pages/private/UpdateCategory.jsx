import UpdateCategoryForm from '@organisms/UpdateCategoryForm';
import { useGlobalSpinnerContext } from '@atoms/GlobalSpinner';
import { useStoreContext } from '@stateMachines/Store';
import Main from '@templates/Main';
import { useActor } from '@xstate/react';
import React, { useEffect } from 'react';
import { useGlobalAlertContext } from '@molecules/GlobalAlert';
import propTypes from 'prop-types';

const UpdateCategory = ({ route: { lastChunk: { request: { body } } } }) => {
  const { updateCategoryService } = useStoreContext();
  const [ updateCategoryFormState ] = useActor(updateCategoryService.children.get('updateCategoryForm'));
  const toggleSpinner = [ ...useGlobalSpinnerContext() ].pop();
  const toggleAlert = [ ...useGlobalAlertContext() ].pop();

  useEffect(() => {
    if (updateCategoryFormState.matches('processing')) toggleSpinner({ type: 'ENABLE' });
    if (!updateCategoryFormState.matches('processing')) toggleSpinner({ type: 'DISABLE' });
    if (updateCategoryFormState.matches('failed')) toggleAlert({
      type: 'ENABLE',
      data: { type: 'error', message: updateCategoryFormState.context.error.message },
    });
    if (updateCategoryFormState.matches('finished')) toggleAlert({
      type: 'ENABLE',
      data: { type: 'success', message: 'updateCategory.txt1' },
    });
  }, [ updateCategoryFormState.value ]);

  return (
    <Main>
      <UpdateCategoryForm
        className='row-start-2 row-span-1 col-start-4 col-span-4 self-center'
        machine={updateCategoryService.children.get('updateCategoryForm')}
        defaultValues={body}
      />
    </Main>
  );
};

UpdateCategory.propTypes = {
  route: propTypes.object.isRequired,
};

export default UpdateCategory;