import UpdateCustomerForm from '@organisms/UpdateCustomerForm';
import { useGlobalSpinnerContext } from '@atoms/GlobalSpinner';
import { useStoreContext } from '@stateMachines/Store';
import Main from '@templates/Main';
import { useActor } from '@xstate/react';
import React, { useEffect } from 'react';
import { useGlobalAlertContext } from '@molecules/GlobalAlert';
import propTypes from 'prop-types';

const UpdateCustomer = ({ route: { lastChunk: { request: { body } } } }) => {
  const { updateCustomerService } = useStoreContext();
  const [ updateCustomerFormState ] = useActor(updateCustomerService.children.get('updateCustomerForm'));
  const toggleSpinner = [ ...useGlobalSpinnerContext() ].pop();
  const toggleAlert = [ ...useGlobalAlertContext() ].pop();

  useEffect(() => {
    if (updateCustomerFormState.matches('processing')) toggleSpinner({ type: 'ENABLE' });
    if (!updateCustomerFormState.matches('processing')) toggleSpinner({ type: 'DISABLE' });
    if (updateCustomerFormState.matches('failed')) toggleAlert({
      type: 'ENABLE',
      data: { type: 'error', message: updateCustomerFormState.context.error.message },
    });
    if (updateCustomerFormState.matches('finished')) toggleAlert({
      type: 'ENABLE',
      data: { type: 'success', message: 'updateCustomer.txt1' },
    });
  }, [ updateCustomerFormState.value ]);

  return (
    <Main>
      <UpdateCustomerForm
        className='row-start-2 row-span-1 col-start-4 col-span-4 self-center'
        machine={updateCustomerService.children.get('updateCustomerForm')}
        defaultValues={body}
      />
    </Main>
  );
};

UpdateCustomer.propTypes = {
  route: propTypes.object.isRequired,
};

export default UpdateCustomer;