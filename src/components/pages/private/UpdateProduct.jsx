import UpdateProductForm from '@organisms/UpdateProductForm';
import { useGlobalSpinnerContext } from '@atoms/GlobalSpinner';
import { useStoreContext } from '@stateMachines/Store';
import Main from '@templates/Main';
import { useActor } from '@xstate/react';
import React, { useEffect } from 'react';
import { useGlobalAlertContext } from '@molecules/GlobalAlert';
import propTypes from 'prop-types';

const UpdateProduct = ({ route: { lastChunk: { request: { body } } } }) => {
  const { updateProductService } = useStoreContext();
  const [ state, send ] = useActor(updateProductService);
  const [ updateProductFormState ] = useActor(updateProductService.children.get('updateProductForm'));
  const toggleSpinner = [ ...useGlobalSpinnerContext() ].pop();
  const toggleAlert = [ ...useGlobalAlertContext() ].pop();

  useEffect(() => {
    send({
      type: 'PROCESS',
    });
  }, []);

  useEffect(() => {
    if (updateProductFormState.matches('processing')) toggleSpinner({ type: 'ENABLE' });
    if (!updateProductFormState.matches('processing')) toggleSpinner({ type: 'DISABLE' });
    if (updateProductFormState.matches('failed')) toggleAlert({
      type: 'ENABLE',
      data: { type: 'error', message: updateProductFormState.context.error.message },
    });
    if (updateProductFormState.matches('finished')) toggleAlert({
      type: 'ENABLE',
      data: { type: 'success', message: 'updateProduct.txt1' },
    });
  }, [ updateProductFormState.value ]);

  return (
    <Main>
      <UpdateProductForm
        defaultValues={body}
        enabledCategories={state.context.enabledCategories}
        className='row-start-2 row-span-1 col-start-4 col-span-4 self-center'
        machine={updateProductService.children.get('updateProductForm')}
      />
    </Main>
  );
};

UpdateProduct.propTypes = {
  route: propTypes.object.isRequired,
};

export default UpdateProduct;