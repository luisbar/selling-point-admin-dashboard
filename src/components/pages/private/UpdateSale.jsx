import UpdateSaleForm from '@organisms/UpdateSaleForm';
import { useGlobalSpinnerContext } from '@atoms/GlobalSpinner';
import { useStoreContext } from '@stateMachines/Store';
import Main from '@templates/Main';
import { useActor } from '@xstate/react';
import React, { useEffect } from 'react';
import { useGlobalAlertContext } from '@molecules/GlobalAlert';
import propTypes from 'prop-types';

const UpdateSale = ({ route: { lastChunk: { request: { body } } } }) => {
  const { updateSaleService } = useStoreContext();
  const [ state, send ] = useActor(updateSaleService);
  const [ updateSaleFormState ] = useActor(updateSaleService.children.get('updateSaleForm'));
  const toggleSpinner = [ ...useGlobalSpinnerContext() ].pop();
  const toggleAlert = [ ...useGlobalAlertContext() ].pop();

  useEffect(() => {
    send({
      type: 'PROCESS',
    });
  }, []);

  useEffect(() => {
    if (updateSaleFormState.matches('processing')) toggleSpinner({ type: 'ENABLE' });
    if (!updateSaleFormState.matches('processing')) toggleSpinner({ type: 'DISABLE' });
    if (updateSaleFormState.matches('failed')) toggleAlert({
      type: 'ENABLE',
      data: { type: 'error', message: updateSaleFormState.context.error.message },
    });
    if (updateSaleFormState.matches('finished')) toggleAlert({
      type: 'ENABLE',
      data: { type: 'success', message: 'updateSale.txt1' },
    });
  }, [ updateSaleFormState.value ]);

  return (
    <Main>
      <UpdateSaleForm
        defaultValues={body}
        enabledCustomers={state.context.enabledCustomers}
        enabledProducts={state.context.enabledProducts}
        className='row-start-2 row-span-1 col-start-1 col-span-10'
        machine={updateSaleService.children.get('updateSaleForm')}
      />
    </Main>
  );
};

UpdateSale.propTypes = {
  route: propTypes.object.isRequired,
};

export default UpdateSale;