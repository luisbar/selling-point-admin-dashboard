import React from 'react';
import Main from '@templates/Main';
import Detail from '@root/components/organisms/Detail';
import moment from 'moment';
import propTypes from 'prop-types';

const ViewCategory = ({ route: { lastChunk: { request: { body } } } }) => {
  const { id, name, description, enabled, createdAt, createdBy, updatedAt, updatedBy } = body;

  return (
    <Main>
      <Detail
        className='row-start-2 row-span-1 col-start-2 col-span-8'
        items={[
          {
            className: 'row-start-1 row-span-1 col-start-1 col-span-1 shadow-none',
            body: {
              className: 'text-4xl font-bold',
              label: id,
            },
          },
          {
            className: 'row-start-2 row-span-1 col-start-1 col-span-2 shadow-none',
            title: {
              label: 'viewCategory.txt2',
            },
            body: {
              label: name,
            },
          },
          {
            className: 'row-start-2 row-span-1 col-start-3 col-span-6 shadow-none',
            title: {
              label: 'viewCategory.txt3',
            },
            body: {
              label: description || '-',
            },
          },
          {
            className: 'row-start-3 row-span-1 col-start-1 col-span-2 dark:bg-dark-200',
            title: {
              label: 'viewCategory.txt4',
            },
            icon: {
              name: enabled ? 'BsCheck' : 'BsX',
              className: enabled ? 'text-success' : 'text-error',
            },
          },
          {
            className: 'row-start-4 row-span-1 col-start-1 col-span-2 dark:bg-dark-200',
            title: {
              label: 'viewCategory.txt5',
            },
            body: {
              label: moment(createdAt).format('DD/MM/YYYY HH:mm:ss'),
            },
            icon: {
              name: 'BsClock',
            },
          },
          {
            className: 'row-start-4 row-span-1 col-start-3 col-span-2 dark:bg-dark-200',
            title: {
              label: 'viewCategory.txt6',
            },
            body: {
              label: createdBy.firstName,
            },
            icon: {
              name: 'BsPerson',
            },
          },
          {
            className: 'row-start-4 row-span-1 col-start-5 col-span-2 dark:bg-dark-200',
            title: {
              label: 'viewCategory.txt7',
            },
            body: {
              label: moment(updatedAt).format('DD/MM/YYYY HH:mm:ss'),
            },
            icon: {
              name: 'BsClock',
            },
          },
          {
            className: 'row-start-4 row-span-1 col-start-7 col-span-2 dark:bg-dark-200',
            title: {
              label: 'viewCategory.txt8',
            },
            body: {
              label: updatedBy ? updatedBy.firstName : '-',
            },
            icon: {
              name: 'BsPerson',
            },
          },
        ]}
      />
    </Main>
  );
};

ViewCategory.propTypes = {
  route: propTypes.object.isRequired,
};

export default ViewCategory;