import React from 'react';
import Main from '@templates/Main';
import Detail from '@root/components/organisms/Detail';
import moment from 'moment';
import propTypes from 'prop-types';

const ViewCustomer = ({ route: { lastChunk: { request: { body } } } }) => {
  const {
    id,
    firstName,
    lastName,
    email,
    nit,
    enabled,
    createdAt,
    createdBy,
    updatedAt,
    updatedBy,
  } = body;

  return (
    <Main>
      <Detail
        className='row-start-2 row-span-1 col-start-2 col-span-8'
        items={[
          {
            className: 'row-start-1 row-span-1 col-start-1 col-span-1 shadow-none',
            body: {
              className: 'text-4xl font-bold',
              label: id,
            },
          },
          {
            className: 'row-start-2 row-span-1 col-start-1 col-span-2 shadow-none',
            title: {
              label: 'viewCustomer.txt2',
            },
            body: {
              label: firstName,
            },
          },
          {
            className: 'row-start-2 row-span-1 col-start-3 col-span-2 shadow-none',
            title: {
              label: 'viewCustomer.txt3',
            },
            body: {
              label: lastName,
            },
          },
          {
            className: 'row-start-3 row-span-1 col-start-1 col-span-2 shadow-none',
            title: {
              label: 'viewCustomer.txt4',
            },
            body: {
              label: email || '-',
            },
          },
          {
            className: 'row-start-3 row-span-1 col-start-3 col-span-2 shadow-none',
            title: {
              label: 'viewCustomer.txt5',
            },
            body: {
              label: nit || '-',
            },
          },
          {
            className: 'row-start-4 row-span-1 col-start-1 col-span-2 dark:bg-dark-200',
            title: {
              label: 'viewCustomer.txt6',
            },
            icon: {
              name: enabled ? 'BsCheck' : 'BsX',
              className: enabled ? 'text-success' : 'text-error',
            },
          },
          {
            className: 'row-start-5 row-span-1 col-start-1 col-span-2 dark:bg-dark-200',
            title: {
              label: 'viewCustomer.txt7',
            },
            body: {
              label: moment(createdAt).format('DD/MM/YYYY HH:mm:ss'),
            },
            icon: {
              name: 'BsClock',
            },
          },
          {
            className: 'row-start-5 row-span-1 col-start-3 col-span-2 dark:bg-dark-200',
            title: {
              label: 'viewCustomer.txt8',
            },
            body: {
              label: createdBy.firstName,
            },
            icon: {
              name: 'BsPerson',
            },
          },
          {
            className: 'row-start-5 row-span-1 col-start-5 col-span-2 dark:bg-dark-200',
            title: {
              label: 'viewCustomer.txt9',
            },
            body: {
              label: moment(updatedAt).format('DD/MM/YYYY HH:mm:ss'),
            },
            icon: {
              name: 'BsClock',
            },
          },
          {
            className: 'row-start-5 row-span-1 col-start-7 col-span-2 dark:bg-dark-200',
            title: {
              label: 'viewCustomer.txt10',
            },
            body: {
              label: updatedBy ? updatedBy.firstName : '-',
            },
            icon: {
              name: 'BsPerson',
            },
          },
        ]}
      />
    </Main>
  );
};

ViewCustomer.propTypes = {
  route: propTypes.object.isRequired,
};

export default ViewCustomer;