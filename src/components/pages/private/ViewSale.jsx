import React from 'react';
import Main from '@templates/Main';
import Detail from '@root/components/organisms/Detail';
import moment from 'moment';
import propTypes from 'prop-types';
import Table from '@organisms/Table';

const ViewSale = ({ route: { lastChunk: { request: { body } } } }) => {
  const {
    id,
    total,
    status,
    customer,
    saleDetail,
    createdAt,
    createdBy,
    updatedAt,
    updatedBy,
  } = body;

  return (
    <Main>
      <div
        className='row-start-2 col-start-1 col-span-10 grid grid-rows-6 grid-cols-10'
      >
        <Detail
          className='row-start-1 row-span-2 col-start-2 col-span-8'
          items={[
            {
              className: 'row-start-1 row-span-3 col-start-1 col-span-1 shadow-none',
              body: {
                className: 'text-4xl font-bold',
                label: id,
              },
            },
            {
              className: 'row-start-4 row-span-3 col-start-1 col-span-2 shadow-none',
              title: {
                label: 'viewSale.txt2',
              },
              body: {
                label: `${customer.firstName} ${customer.lastName}`,
              },
            },
            {
              className: 'row-start-4 row-span-3 col-start-3 col-span-2 shadow-none',
              title: {
                label: 'viewSale.txt3',
              },
              body: {
                label: status,
              },
            },
            {
              className: 'row-start-4 row-span-3 col-start-5 col-span-2 shadow-none',
              title: {
                label: 'viewSale.txt4',
              },
              body: {
                label: String(total),
              },
            },
          ]}
        />
        <Table
          className='row-start-3 row-span-3 col-start-2 col-span-8'
          columns={[
            { key: 'product.name', value: 'viewSale.txt5', type: 'text' },
            { key: 'quantity', value: 'viewSale.txt6', type: 'text' },
            { key: 'product.price', value: 'viewSale.txt7', type: 'text' },
            { key: 'subtotal', value: 'viewSale.txt8', type: 'text' },
          ]}
          rows={saleDetail}
        />
        <Detail
          className='row-start-6 row-span-1 col-start-2 col-span-8'
          items={[
            {
              className: 'row-start-1 row-span-6 col-start-1 col-span-2 dark:bg-dark-200',
              title: {
                label: 'viewSale.txt9',
              },
              body: {
                label: moment(createdAt).format('DD/MM/YYYY HH:mm:ss'),
              },
              icon: {
                name: 'BsClock',
              },
            },
            {
              className: 'row-start-1 row-span-6 col-start-3 col-span-2 dark:bg-dark-200',
              title: {
                label: 'viewSale.txt10',
              },
              body: {
                label: createdBy.firstName,
              },
              icon: {
                name: 'BsPerson',
              },
            },
            {
              className: 'row-start-1 row-span-6 col-start-5 col-span-2 dark:bg-dark-200',
              title: {
                label: 'viewSale.txt11',
              },
              body: {
                label: moment(updatedAt).format('DD/MM/YYYY HH:mm:ss'),
              },
              icon: {
                name: 'BsClock',
              },
            },
            {
              className: 'row-start-1 row-span-6 col-start-7 col-span-2 dark:bg-dark-200',
              title: {
                label: 'viewSale.txt12',
              },
              body: {
                label: updatedBy ? updatedBy.firstName : '-',
              },
              icon: {
                name: 'BsPerson',
              },
            },
          ]}
        />
      </div>
    </Main>
  );
};

ViewSale.propTypes = {
  route: propTypes.object.isRequired,
};

export default ViewSale;