import CreateCustomerForm from '@organisms/CreateCustomerForm';
import { useGlobalSpinnerContext } from '@atoms/GlobalSpinner';
import { useStoreContext } from '@stateMachines/Store';
import Main from '@templates/Main';
import { useActor } from '@xstate/react';
import React, { useEffect } from 'react';
import { useGlobalAlertContext } from '@molecules/GlobalAlert';

const CreateCustomer = () => {
  const { createCustomerService } = useStoreContext();
  const [ createCustomerFormState ] = useActor(createCustomerService.children.get('createCustomerForm'));
  const toggleSpinner = [ ...useGlobalSpinnerContext() ].pop();
  const toggleAlert = [ ...useGlobalAlertContext() ].pop();

  useEffect(() => {
    if (createCustomerFormState.matches('processing')) toggleSpinner({ type: 'ENABLE' });
    if (!createCustomerFormState.matches('processing')) toggleSpinner({ type: 'DISABLE' });
    if (createCustomerFormState.matches('failed')) toggleAlert({
      type: 'ENABLE',
      data: { type: 'error', message: createCustomerFormState.context.error.message },
    });
    if (createCustomerFormState.matches('finished')) toggleAlert({
      type: 'ENABLE',
      data: { type: 'success', message: 'createCustomer.txt1' },
    });
  }, [ createCustomerFormState.value ]);

  return (
    <Main>
      <CreateCustomerForm
        className='row-start-2 row-span-1 col-start-4 col-span-4 self-center'
        machine={createCustomerService.children.get('createCustomerForm')}
      />
    </Main>
  );
};

export default CreateCustomer;