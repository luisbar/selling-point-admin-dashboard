import Image from '@atoms/Image';
import Text from '@atoms/Text';
import Centered from '@templates/Centered';
import React from 'react';
import logo from '@images/logo-dark.svg';

const Splash = () => (
  <Centered
    className='bg-light-100'
  >
    <Image
      src={logo}
      className='w-1/3'
    />
    <Text
      as='h3'
      label='splash.txt1'
    />
  </Centered>
);

export default Splash;
