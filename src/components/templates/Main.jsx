import propTypes from 'prop-types';
import React from 'react';
import SideMenu from '@organisms/SideMenu';
import Checkbox from '@atoms/Checkbox';
import Dropdown from '@atoms/Dropdown';
import { useStoreContext } from '@root/stateMachines/Store';
import { useInternationalizationContext } from '@root/internationalization/InternationalizationProvider';
import { useActor } from '@xstate/react';

const Main = ({ children }) => {
  const { homeService } = useStoreContext();
  const [ checkboxState ] = useActor(homeService.children.get('changeThemeCheckbox'));
  const [ language, setLanguage ] = useInternationalizationContext();

  const onChangeLanguage = ({ target }) => {
    setLanguage(target.value);
  };

  return (
    <div
      className='w-full h-full flex flex-row dark:bg-dark-100'
    >
      <SideMenu
        items={[
          {
            path: '/',
            value: 'sideMenu.txt1',
          },
          {
            path: '/sales',
            value: 'sideMenu.txt5',
          },
          {
            path: '/customers',
            value: 'sideMenu.txt4',
          },
          {
            path: '/products',
            value: 'sideMenu.txt3',
          },
          {
            path: '/categories',
            value: 'sideMenu.txt2',
          },
        ]}
      />
      <div
        className='w-full grid grid-rows-main grid-cols-10 gap-y-5 gap-x-5 p-5'
      >
        <Dropdown
          className='row-start-1 col-start-9 justify-self-end self-end'
          machine={homeService.children.get('languagesDropdown')}
          select={{
            value: language,
            options: [
              {
                key: 'en',
                value: 'main.txt1',
              },
              {
                key: 'es',
                value: 'main.txt2',
              },
            ],
            onChange: onChangeLanguage,
          }}
          text={{
            label: 'main.txt3',
          }}
        />
        <Checkbox
          label='main.txt4'
          className='row-start-1 col-start-10 justify-self-end self-end'
          data={{
            type: 'CHANGE_THEME',
          }}
          machine={homeService.children.get('changeThemeCheckbox')}
          value={checkboxState.value === 'checked'}
        />
        {children}
      </div>
    </div>
  );
};

Main.propTypes = {
  children: propTypes.node.isRequired,
};

export default Main;