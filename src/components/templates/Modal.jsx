import React from 'react';
import propTypes from 'prop-types';
import { useHistory } from 'react-navi';
import { IoMdClose } from 'react-icons/io';

const Modal = ({ children }) => {
  const history = useHistory();

  const onClose = () => {
    history.goBack();
  };

  return (
    <div
      className='h-full w-full flex flex-col items-center justify-center'
    >
      <IoMdClose
        className='absolute top-1 right-1 cursor-pointer text-4xl text-accent-100'
        onClick={onClose}
      />
      {children}
    </div>
  );
};

Modal.propTypes = {
  children: propTypes.node.isRequired,
};

export default Modal;