import React, { createContext, useState, useContext } from 'react';
import { IntlProvider } from 'react-intl';
import { getLanguage, getMessage } from '@internationalization';
import propTypes from 'prop-types';

const InternationalizationContext = createContext([]);

const InternationalizationProvider = ({ children }) => {
  const [ language, setLanguage ] = useState(navigator.language.split(
    /[-_]/,
  )[0]);

  const onError = (error) => {
    if (error.code === 'MISSING_TRANSLATION') return console.warn(error.message);

    throw error;
  };

  return (
    <InternationalizationContext.Provider
      value={[ language, setLanguage ]}
    >
      <IntlProvider
        locale={getLanguage(language)}
        messages={getMessage(language)}
        onError={onError}
      >
        {children}
      </IntlProvider>
    </InternationalizationContext.Provider>
  );
};

InternationalizationProvider.propTypes = {
  children: propTypes.node.isRequired,
};

export default InternationalizationProvider;
export const useInternationalizationContext = () => useContext(
  InternationalizationContext,
);