import getItemFromCookies from '@utils/getItemFromCookies';
import decodeJWT from 'jwt-decode';
import moment from 'moment';

export default () => {
  const token = getItemFromCookies('token');
  if (!token) return false;

  const decodedToken = decodeJWT(token);
  const expirationDate = moment.unix(decodedToken.exp);
  const currentDate = moment();
  return currentDate.isBefore(expirationDate);
};
