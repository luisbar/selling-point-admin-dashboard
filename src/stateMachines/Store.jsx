import React, { createContext, useContext } from 'react';
import { useInterpret } from '@xstate/react';
import homeMachine from '@stateMachines/pages/home';
import loginMachine from '@stateMachines/pages/login';
import categoriesMachine from '@stateMachines/pages/categories';
import createCategoryMachine from '@stateMachines/pages/createCategory';
import updateCategoryMachine from '@stateMachines/pages/updateCategory';
import productsMachine from '@stateMachines/pages/products';
import createProductMachine from '@stateMachines/pages/createProduct';
import updateProductMachine from '@stateMachines/pages/updateProduct';
import customersMachine from '@stateMachines/pages/customers';
import createCustomerMachine from '@stateMachines/pages/createCustomer';
import updateCustomerMachine from '@stateMachines/pages/updateCustomer';
import salesMachine from '@stateMachines/pages/sales';
import createSaleMachine from '@stateMachines/pages/createSale';
import updateSaleMachine from '@stateMachines/pages/updateSale';
import propTypes from 'prop-types';

export const StoreContext = createContext([]);
const StoreProvider = ({ children }) => {
  const homeService = useInterpret(homeMachine, { devTools: process.env.ENV === 'development' });
  const loginService = useInterpret(loginMachine, { devTools: process.env.ENV === 'development' });
  const categoriesService = useInterpret(categoriesMachine, { devTools: process.env.ENV === 'development' });
  const createCategoryService = useInterpret(createCategoryMachine, { devTools: process.env.ENV === 'development' });
  const updateCategoryService = useInterpret(updateCategoryMachine, { devTools: process.env.ENV === 'development' });
  const productsService = useInterpret(productsMachine, { devTools: process.env.ENV === 'development' });
  const createProductService = useInterpret(createProductMachine, { devTools: process.env.ENV === 'development' });
  const updateProductService = useInterpret(updateProductMachine, { devTools: process.env.ENV === 'development' });
  const customersService = useInterpret(customersMachine, { devTools: process.env.ENV === 'development' });
  const createCustomerService = useInterpret(createCustomerMachine, { devTools: process.env.ENV === 'development' });
  const updateCustomerService = useInterpret(updateCustomerMachine, { devTools: process.env.ENV === 'development' });
  const salesService = useInterpret(salesMachine, { devTools: process.env.ENV === 'development' });
  const createSaleService = useInterpret(createSaleMachine, { devTools: process.env.ENV === 'development' });
  const updateSaleService = useInterpret(updateSaleMachine, { devTools: process.env.ENV === 'development' });

  return (
    <StoreContext.Provider
      value={{
        homeService,
        loginService,
        categoriesService,
        createCategoryService,
        updateCategoryService,
        productsService,
        createProductService,
        updateProductService,
        customersService,
        createCustomerService,
        updateCustomerService,
        salesService,
        createSaleService,
        updateSaleService,
      }}
    >
      {children}
    </StoreContext.Provider>
  );
};

StoreProvider.propTypes = {
  children: propTypes.node.isRequired,
};

export default StoreProvider;
export const useStoreContext = () => useContext(
  StoreContext,
);