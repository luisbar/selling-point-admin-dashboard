import React from 'react';
import { produce } from 'immer';
import { machineDefinition } from '@stateMachines/atoms/checkbox';
import { createMachine } from 'xstate';
import { createModel } from '@xstate/test';
import { render, cleanup, fireEvent } from '@root/jest.utils';
import Checkbox from '@atoms/Checkbox';

const getMachineDefinitionWithTests = () => produce(machineDefinition, (draft) => {
  draft.states.unchecked.meta = {
    test: ({ getByTestId }) => {
      expect(getByTestId('checkbox-child-3')).toHaveClass('w-8 h-4 rounded-md duration-500 bg-dark-300');
      expect(getByTestId('checkbox-child-3.1')).toHaveClass('bg-light-100 w-4 h-4 rounded-full duration-500');
    },
  };
  draft.states.checked.meta = {
    test: ({ getByTestId }) => {
      expect(getByTestId('checkbox-child-3')).toHaveClass('w-8 h-4 rounded-md duration-500 bg-accent-100');
      expect(getByTestId('checkbox-child-3.1')).toHaveClass('bg-light-100 w-4 h-4 rounded-full duration-500 transform translate-x-full');
    },
  };
});

const getEvents = () => ({
  TOGGLE: {
    exec: ({ getByTestId }) => {
      fireEvent.click(getByTestId('checkbox-container'));
    },
    cases: [ {} ],
  },
});

describe('checkbox', () => {
  const machine = createMachine(getMachineDefinitionWithTests(), {
    actions: {
      sendParent: () => {},
    },
  });
  const machineModel = createModel(machine)
    .withEvents(getEvents());
  const testPlans = machineModel.getSimplePathPlans();

  testPlans.forEach((plan) => {
    describe(plan.description, () => {
      afterEach(cleanup);

      plan.paths.forEach((path) => {
        it(path.description, () => {
          const rendered = render(
            <Checkbox
              test
              label='main.txt1'
              data={{}}
              machine={machine}
            />,
            { locale: 'en' },
          );

          return path.test(rendered);
        });
      });
    });
  });

  describe('coverage', () => {
    it('should have full coverage', () => {
      machineModel.testCoverage();
    });
  });
});
