import requester from '@utils/requester';
import { assign } from '@xstate/immer';
import { createMachine } from 'xstate';

export const machineDefinition = {
  id: 'datalist',
  initial: 'idle',
  context: {
    options: [],
    error: null,
  },
  states: {
    idle: {
      on: {
        PROCESS: [
          {
            target: 'processing',
          },
        ],
        CLEAN_OPTIONS: [
          {
            actions: [ 'cleanOptions' ],
            target: 'idle',
          },
        ],
        SHOW_ERROR: [
          {
            actions: [ 'showError' ],
            target: 'showingError',
          },
        ],
        CLEAN_ERROR: [
          {
            actions: [ 'cleanError' ],
            target: 'cleaningError',
          },
        ],
        DISABLE: [
          {
            target: 'disabled',
          },
        ],
      },
    },
    processing: {
      invoke: [
        {
          id: 'getEnabledCustomers',
          src: 'getEnabledCustomers',
          onDone: {
            actions: [ 'saveEnabledCustomers' ],
            target: 'finished',
          },
          onError: {
            actions: [ 'saveError' ],
            target: 'failed',
          },
        },
      ],
    },
    finished: {
      after: {
        1: {
          target: 'idle',
        },
      },
    },
    failed: {
      after: {
        1: {
          target: 'idle',
        },
      },
    },
    showingError: {
      after: {
        1: {
          target: 'idle',
        },
      },
    },
    cleaningError: {
      after: {
        1: {
          target: 'idle',
        },
      },
    },
    disabled: {
      on: {
        ENABLE: [
          {
            target: 'idle',
          },
        ],
        CLEAN_OPTIONS: [
          {
            actions: [ 'cleanOptions' ],
            target: 'disabled',
          },
        ],
      },
    },
  },
};

const machineOptions = {
  actions: {
    saveError: assign((context, event) => { context.error = event.data; }),
    saveEnabledCustomers: assign((context, event) => {
      context.options = event.data;
    }),
    cleanOptions: assign((context) => { context.options = []; }),
    showError: assign((context, event) => { context.error = event.data.error; }),
    cleanError: assign((context) => { context.error = null; }),
  },
  services: {
    getEnabledCustomers: async (context, { data: { filter } }) => {
      const isNumber = new RegExp(/[0-9]/).exec(filter);
      let args = {
        take: 10,
        skip: 0,
        orderBy: {
          firstName: 'desc',
        },
      };

      if (!isNumber) args = {
        ...args,
        where: {
          enabled: {
            equals: true,
          },
          OR: [
            { firstName: { in: filter.split(' ') } },
            { lastName: { in: filter.split(' ') } },
            { email: { in: filter.split(' ') } },
            { firstName: { contains: filter } },
            { lastName: { contains: filter } },
            { email: { contains: filter } },
          ],
        },
      };

      if (isNumber) args = {
        ...args,
        where: {
          id: { in: [ Number(filter) ] },
        },
      };

      const graphqlQuery = {
        query: `query findCustomers($args: FindManyCustomerArgs!) {
          findCustomers(args: $args) {
            id
            firstName
            lastName
          }
        }`,
        variables: {
          args,
        },
      };
      const response = await requester.post(`/api/graphql`, graphqlQuery);
      if (!response.data.data.findCustomers.length) throw new Error('errors.response.empty');
      return response.data.data.findCustomers.map((customer) => ({
        key: customer.id,
        value: `${customer.firstName} ${customer.lastName}`,
      }));
    },
  },
};

export default createMachine(machineDefinition, machineOptions);