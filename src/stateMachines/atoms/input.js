import { assign } from '@xstate/immer';
import { createMachine } from 'xstate';

export const machineDefinition = {
  id: 'input',
  initial: 'enabled',
  context: {
    error: null,
  },
  states: {
    enabled: {
      on: {
        DISABLE: [
          {
            target: 'disabled',
          },
        ],
        SHOW_ERROR: [
          {
            actions: [ 'showError' ],
            target: 'showingError',
          },
        ],
        CLEAN_ERROR: [
          {
            actions: [ 'cleanError' ],
            target: 'cleaningError',
          },
        ],
      },
    },
    disabled: {
      on: {
        ENABLE: [
          {
            target: 'enabled',
          },
        ],
      },
    },
    showingError: {
      after: {
        1: {
          target: 'enabled',
        },
      },
    },
    cleaningError: {
      after: {
        1: {
          target: 'enabled',
        },
      },
    },
  },
};

const machineOptions = {
  actions: {
    showError: assign((context, event) => { context.error = event.data.error; }),
    cleanError: assign((context) => { context.error = null; }),
  },
};

export default createMachine(machineDefinition, machineOptions);