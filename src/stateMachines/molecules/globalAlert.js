import { assign } from '@xstate/immer';
import { createMachine } from 'xstate';

export const machineDefinition = {
  id: 'globalAlert',
  initial: 'disabled',
  context: {
    type: null,
    message: null,
  },
  states: {
    disabled: {
      on: {
        ENABLE: {
          target: 'enabled',
          actions: [ 'saveData' ],
        },
      },
    },
    enabled: {
      after: {
        3000: [
          {
            target: 'disabled',
          },
        ],
      },
    },
  },
};

const machineOptions = {
  actions: {
    saveData: assign((context, event) => {
      context.type = event.data.type;
      context.message = event.data.message;
    }),
  },
};

export default createMachine(machineDefinition, machineOptions);