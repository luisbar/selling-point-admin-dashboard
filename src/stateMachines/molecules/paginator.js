import { createMachine, sendParent, send, actions } from 'xstate';
import { assign } from '@xstate/immer';
import buttonMachine from '@stateMachines/atoms/button';

const CHILDREN = [ 'previousButton', 'nextButton' ];

export const machineDefinition = {
  id: 'paginator',
  initial: 'enabled',
  context: {
    pageNumber: null,
    pageSize: null,
  },
  invoke: [
    {
      id: 'previousButton',
      src: buttonMachine,
    },
    {
      id: 'nextButton',
      src: buttonMachine,
    },
  ],
  states: {
    enabled: {
      entry: [ 'toggleButtons' ],
      on: {
        UPDATE_PAGINATION: {
          actions: [ 'updatePagination' ],
          target: 'enabled',
        },
        PREVIOUS: {
          actions: [ 'sendParentPreviousPageNumber' ],
          target: 'enabled',
        },
        NEXT: {
          actions: [ 'sendParentNextPageNumber' ],
          target: 'enabled',
        },
        DISABLE: {
          target: 'disabled',
        },
      },
    },
    disabled: {
      entry: [ 'disableAll' ],
      on: {
        ENABLE: {
          target: 'enabled',
        },
      },
      exit: [ 'enableAll' ],
    },
  },
};

const machineOptions = {
  actions: {
    toggleButtons: actions.pure(() => [
      actions.choose(
        [
          {
            cond: 'thereIsNotPreviousPage',
            actions: [ 'disablePreviousButton' ],
          },
          {
            cond: 'thereIsPreviousPage',
            actions: [ 'enablePreviousButton' ],
          },
        ],
      ),
    ]),
    updatePagination: assign((
      context, { data: { pagination: { pageNumber, pageSize } } },
    ) => {
      context.pageNumber = pageNumber;
      context.pageSize = pageSize;
    }),
    sendParentPreviousPageNumber: sendParent(({ pageNumber }) => ({ type: 'PROCESS', data: { pageNumber: pageNumber - 1, direction: 'PREVIOUS' } })),
    sendParentNextPageNumber: sendParent(({ pageNumber }) => ({ type: 'PROCESS', data: { pageNumber: pageNumber + 1, direction: 'NEXT' } })),
    disableAll: actions.pure(() => CHILDREN.map((child) => send({ type: 'DISABLE' }, { to: child }))),
    disablePreviousButton: send({ type: 'DISABLE' }, { to: 'previousButton' }),
    disableNextButton: send({ type: 'DISABLE' }, { to: 'nextButton' }),
    enableAll: actions.pure(() => CHILDREN.map((child) => send({ type: 'ENABLE' }, { to: child }))),
    enablePreviousButton: send({ type: 'ENABLE' }, { to: 'previousButton' }),
    enableNextButton: send({ type: 'ENABLE' }, { to: 'nextButton' }),
  },
  guards: {
    thereIsNotPreviousPage: (context) => context.pageNumber <= 0,
    thereIsPreviousPage: (context) => context.pageNumber > 0,
  },
};

export default createMachine(machineDefinition, machineOptions);