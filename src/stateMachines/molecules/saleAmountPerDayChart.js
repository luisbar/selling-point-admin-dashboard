import { assign } from '@xstate/immer';
import { createMachine, actions, send } from 'xstate';
import requester from '@utils/requester';
import inputMachine from '@stateMachines/atoms/input';
import buttonMachine from '@stateMachines/atoms/button';
import moment from 'moment';

const CHILDREN_TO_DISABLED = [ 'startDateInput', 'endDateInput', 'filterButton' ];

export const machineDefinition = {
  id: 'saleAmountPerDayChart',
  initial: 'idle',
  context: {
    data: {
      labels: [],
      datasets: [],
    },
    options: {},
    error: {},
  },
  invoke: [
    {
      id: 'startDateInput',
      src: inputMachine,
    },
    {
      id: 'endDateInput',
      src: inputMachine,
    },
    {
      id: 'filterButton',
      src: buttonMachine,
    },
  ],
  states: {
    idle: {
      on: {
        PROCESS: [
          {
            target: 'processing',
          },
        ],
      },
    },
    processing: {
      entry: [ 'disableAll' ],
      invoke: {
        id: 'getSaleAmountPerDay',
        src: 'getSaleAmountPerDay',
        onDone: {
          actions: [ 'saveSalesByStatus' ],
          target: 'finished',
        },
        onError: {
          actions: [ 'saveError' ],
          target: 'failed',
        },
      },
      exit: [ 'enableAll' ],
    },
    finished: {
      on: {
        PROCESS: [
          {
            target: 'processing',
          },
        ],
      },
    },
    failed: {
      after: {
        1: {
          target: 'idle',
        },
      },
    },
  },
};

const machineOptions = {
  actions: {
    disableAll: actions.pure(() => CHILDREN_TO_DISABLED.map((child) => send({ type: 'DISABLE' }, { to: child, delay: 1 }))),
    enableAll: actions.pure(() => CHILDREN_TO_DISABLED.map((child) => send({ type: 'ENABLE' }, { to: child }))),
    saveError: assign((context, event) => { context.error = event.data; }),
    saveSalesByStatus: assign((context, event) => {
      context.data.labels = event.data.map(({ createdAt }) => moment(createdAt).format('DD-MM-YYYY HH:mm'));
      context.data.datasets = [ {
        label: 'sales amount',
        data: event.data.map(({ _sum }) => _sum.total),
        backgroundColor: 'rgba(255, 99, 132, 0.5)',
      } ];
    }),
  },
  services: {
    getSaleAmountPerDay: async (context, { data: { startDate, endDate } }) => {
      const args = {
        by: [ 'createdAt' ],
        _sum: {
          total: true,
        },
        where: {
          AND: [
            {
              createdAt: {
                gte: startDate,
              },
            },
            {
              createdAt: {
                lte: endDate,
              },
            },
          ],
        },
      };

      const graphqlQuery = {
        query: `query groupBySales ($args: SaleGroupByArgs!) {
          groupBySales (args: $args) {
            createdAt
            _sum {
              total
            }
          }
        }`,
        variables: {
          args,
        },
      };

      const response = await requester.post(`/api/graphql`, graphqlQuery);
      return response.data.data.groupBySales;
    },
  },
};

export default createMachine(machineDefinition, machineOptions);