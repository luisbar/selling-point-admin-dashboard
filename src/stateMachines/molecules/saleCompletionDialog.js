import { assign } from '@xstate/immer';
import { createMachine, sendParent, actions, send } from 'xstate';
import buttonMachine from '@stateMachines/atoms/button';
import requester from '@utils/requester';

const CHILDREN_TO_DISABLED = [ 'confirmationButton' ];

export const machineDefinition = {
  id: 'saleCompletionDialog',
  initial: 'idle',
  context: {},
  invoke: [
    {
      id: 'confirmationButton',
      src: buttonMachine,
    },
  ],
  states: {
    idle: {
      on: {
        PROCESS: {
          target: 'processing',
        },
      },
    },
    processing: {
      entry: [ 'disableAll' ],
      invoke: [
        {
          id: 'completeSale',
          src: 'completeSale',
          onDone: {
            actions: [ 'sendResponseParent' ],
            target: 'finished',
          },
          onError: {
            actions: [ 'saveError' ],
            target: 'failed',
          },
        },
      ],
      exit: [ 'enableAll' ],
    },
    finished: {
      on: {
        PROCESS: [
          {
            target: 'processing',
          },
        ],
      },
    },
    failed: {
      after: {
        1: {
          target: 'idle',
        },
      },
    },
  },
};

const machineOptions = {
  actions: {
    disableAll: actions.pure(() => CHILDREN_TO_DISABLED.map((child) => send({ type: 'DISABLE' }, { to: child, delay: 1 }))),
    enableAll: actions.pure(() => CHILDREN_TO_DISABLED.map((child) => send({ type: 'ENABLE' }, { to: child }))),
    sendResponseParent: sendParent((context, event) => ({ type: 'UPDATE_SALE', data: event.data })),
    saveError: assign((context, event) => { context.error = event.data; }),
  },
  services: {
    completeSale: async (context, { data }) => {
      try {
        const body = {
          status: {
            set: 'completed',
          },
        };
        const graphqlQuery = {
          query: `mutation updateSale($body: SaleUpdateInput!, $id: Int!) {
            updateSale(body: $body, id: $id) {
              id
              status
            }
          }`,
          variables: {
            body,
            id: Number(data),
          },
        };
        const response = await requester.post(`/api/graphql`, graphqlQuery);
        return response.data.data.updateSale;
      } catch (error) {
        throw error.response.data.errors[0];
      }
    },
  },
};

export default createMachine(machineDefinition, machineOptions);