import { assign } from '@xstate/immer';
import { createMachine, actions, send } from 'xstate';
import requester from '@utils/requester';
import inputMachine from '@stateMachines/atoms/input';
import buttonMachine from '@stateMachines/atoms/button';

const CHILDREN_TO_DISABLED = [ 'startDateInput', 'endDateInput', 'filterButton' ];

export const machineDefinition = {
  id: 'saleByStatusChart',
  initial: 'idle',
  context: {
    data: {
      labels: [],
      datasets: [],
    },
    options: {},
    error: {},
  },
  invoke: [
    {
      id: 'startDateInput',
      src: inputMachine,
    },
    {
      id: 'endDateInput',
      src: inputMachine,
    },
    {
      id: 'filterButton',
      src: buttonMachine,
    },
  ],
  states: {
    idle: {
      on: {
        PROCESS: [
          {
            target: 'processing',
          },
        ],
      },
    },
    processing: {
      entry: [ 'disableAll' ],
      invoke: {
        id: 'geSalesByStatus',
        src: 'geSalesByStatus',
        onDone: {
          actions: [ 'saveSalesByStatus' ],
          target: 'finished',
        },
        onError: {
          actions: [ 'saveError' ],
          target: 'failed',
        },
      },
      exit: [ 'enableAll' ],
    },
    finished: {
      on: {
        PROCESS: [
          {
            target: 'processing',
          },
        ],
      },
    },
    failed: {
      after: {
        1: {
          target: 'idle',
        },
      },
    },
  },
};

const machineOptions = {
  actions: {
    disableAll: actions.pure(() => CHILDREN_TO_DISABLED.map((child) => send({ type: 'DISABLE' }, { to: child, delay: 1 }))),
    enableAll: actions.pure(() => CHILDREN_TO_DISABLED.map((child) => send({ type: 'ENABLE' }, { to: child }))),
    saveError: assign((context, event) => { context.error = event.data; }),
    saveSalesByStatus: assign((context, event) => {
      const total = event.data.reduce((accumulator, current) => accumulator + current._count.id, 0);
      context.data.labels = event.data.map(({ status }) => status);
      context.data.datasets = [ {
        data: event.data.map(({ _count }) => ((_count.id * 100) / total).toFixed(2)),
        backgroundColor: [
          'rgba(255, 99, 132, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(255, 206, 86, 0.2)',
        ],
      } ];
    }),
  },
  services: {
    geSalesByStatus: async (context, { data: { startDate, endDate } }) => {
      const args = {
        by: [ 'status' ],
        _count: {
          id: true,
        },
        where: {
          AND: [
            {
              createdAt: {
                gte: startDate,
              },
            },
            {
              createdAt: {
                lte: endDate,
              },
            },
          ],
        },
      };

      const graphqlQuery = {
        query: `query groupBySales ($args: SaleGroupByArgs!) {
          groupBySales (args: $args) {
            status
            _count {
              id
            }
          }
        }`,
        variables: {
          args,
        },
      };

      const response = await requester.post(`/api/graphql`, graphqlQuery);
      return response.data.data.groupBySales;
    },
  },
};

export default createMachine(machineDefinition, machineOptions);