import { createMachine, send, actions, spawn } from 'xstate';
import { assign } from '@xstate/immer';
import buttonMachine from '@stateMachines/atoms/button';
import inputMachine from '@stateMachines/atoms/input';
import customerDatalistMachine from '@root/stateMachines/atoms/customerDatalist';
import schemaValidator from '@utils/schemaValidator';
import requester from '@utils/requester';
import * as ramda from 'ramda';

const INPUTS_CHILDREN = [ 'customerDatalist', 'totalInput', 'givenMoneyInput', 'changeInput' ];
const CHILDREN_TO_DISABLED = [ ...INPUTS_CHILDREN, 'saveButton' ];

export const machineDefinition = {
  id: 'createSaleForm',
  initial: 'idle',
  context: {
    error: {},
    inputs: [],
  },
  invoke: [
    {
      id: 'totalInput',
      src: inputMachine,
    },
    {
      id: 'givenMoneyInput',
      src: inputMachine,
    },
    {
      id: 'changeInput',
      src: inputMachine,
    },
    {
      id: 'customerDatalist',
      src: customerDatalistMachine,
    },
    {
      id: 'saveButton',
      src: buttonMachine,
    },
  ],
  states: {
    idle: {
      on: {
        SPAWN_INPUTS: [
          {
            actions: [ 'spawnInputs' ],
            target: 'idle',
          },
        ],
        PROCESS: [
          {
            cond: 'dataIsValid',
            actions: [ 'cleanErrorOnInputs' ],
            target: 'processing',
          },
          {
            actions: [ 'cleanErrorOnInputs', 'validateAndShowErrorOnInputs' ],
            target: 'idle',
          },
        ],
      },
    },
    processing: {
      entry: [ 'disableAll' ],
      invoke: [
        {
          id: 'createSale',
          src: 'createSale',
          onDone: {
            actions: [ 'cleanErrorOnInputs' ],
            target: 'finished',
          },
          onError: {
            actions: [ 'saveError', 'showBackendErrorsOnInputs' ],
            target: 'failed',
          },
        },
      ],
      exit: [ 'enableAll' ],
    },
    finished: {
      after: {
        1: {
          target: 'idle',
        },
      },
    },
    failed: {
      after: {
        1: {
          target: 'idle',
        },
      },
    },
  },
};

const machineOptions = {
  actions: {
    disableAll: actions.pure(({ inputs }) => {
      const dynamicAndStaticInputs = [
        ...inputs.map((input) => input.id),
        ...CHILDREN_TO_DISABLED,
      ];
      return dynamicAndStaticInputs
        .map((child) => send({ type: 'DISABLE' }, { to: child, delay: 1 }));
    }),
    enableAll: actions.pure(({ inputs }) => {
      const dynamicAndStaticInputs = [
        ...inputs.map((input) => input.id),
        ...CHILDREN_TO_DISABLED,
      ];
      return dynamicAndStaticInputs
        .map((child) => send({ type: 'ENABLE' }, { to: child, delay: 1 }));
    }),
    saveError: assign((context, event) => { context.error = event.data; }),
    cleanErrorOnInputs: actions.pure(() => INPUTS_CHILDREN.map((input) => send({ type: 'CLEAN_ERROR' }, { to: input }))),
    spawnInputs: actions.pure((context, event) => {
      ramda.forEachObjIndexed((value) => {
        value.forEach((enabledProduct) => {
          context.inputs.push(spawn(inputMachine, enabledProduct.name));
        });
      }, event.data);
    }),
    validateAndShowErrorOnInputs: actions
      .pure((context, event) => schemaValidator
        .validateCreateSaleData(event.data)
        .map((errorMessage) => {
          const targetInput = INPUTS_CHILDREN.filter((input) => errorMessage.includes(input.replace(/.(Input||Datalist)$/, '')))[0];
          return send(
            { type: 'SHOW_ERROR', data: { error: errorMessage } },
            { to: targetInput, delay: 1 },
          );
        })),
    showBackendErrorsOnInputs: actions
      .pure(({ error: { fields } }) => Object.entries(fields)
        .map(([ field, errorMessage ]) => {
          const targetInput = INPUTS_CHILDREN.filter((input) => field.includes(input.replace(/.(Input||Datalist)$/, '')))[0];
          return send(
            { type: 'SHOW_ERROR', data: { error: errorMessage } },
            { to: targetInput, delay: 1 },
          );
        })),
  },
  guards: {
    dataIsValid: (context, event) => !Array.isArray(
      schemaValidator.validateCreateSaleData(event.data),
    ),
  },
  services: {
    createSale: async (context, { data }) => {
      try {
        const validatedData = schemaValidator.validateCreateSaleData(data);
        const saleCreateInput = {
          total: validatedData.total,
          customer: {
            connect: {
              id: validatedData.customer,
            },
          },
          saleDetail: {
            createMany: {
              data: validatedData.saleDetail,
            },
          },
        };
        const graphqlQuery = {
          query: `mutation createSale($saleCreateInput: SaleCreateInput!) {
            createSale(saleCreateInput: $saleCreateInput) {
              id
            }
          }`,
          variables: {
            saleCreateInput,
          },
        };
        const response = await requester.post(`/api/graphql`, graphqlQuery);
        return response.data.data.createSale;
      } catch (error) {
        throw error.response.data.errors[0];
      }
    },
  },
};

export default createMachine(machineDefinition, machineOptions);