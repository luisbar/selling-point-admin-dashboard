import { createMachine, sendParent, actions, send } from 'xstate';
import buttonMachine from '@stateMachines/atoms/button';
import inputMachine from '@stateMachines/atoms/input';
import dropdownMachine from '@stateMachines/atoms/dropdown';
import schemaValidator from '@utils/schemaValidator';

const INPUTS_CHILDREN = [ 'pageSizeDropdown', 'filterInput' ];
const CHILDREN_TO_DISABLED = [ ...INPUTS_CHILDREN, 'filterButton' ];

export const machineDefinition = {
  id: 'filterForm',
  initial: 'enabled',
  invoke: [
    {
      id: 'pageSizeDropdown',
      src: dropdownMachine,
    },
    {
      id: 'filterInput',
      src: inputMachine,
    },
    {
      id: 'filterButton',
      src: buttonMachine,
    },
  ],
  states: {
    enabled: {
      on: {
        PROCESS: [
          {
            cond: 'dataIsValid',
            actions: [ 'cleanErrorOnInputs', 'sendDataToParent' ],
            target: 'enabled',
          },
          {
            actions: [ 'cleanErrorOnInputs', 'showErrorOnInputs' ],
            target: 'enabled',
          },
        ],
        DISABLE: {
          target: 'disabled',
        },
      },
    },
    disabled: {
      entry: [ 'disableAll' ],
      on: {
        ENABLE: {
          target: 'enabled',
        },
      },
      exit: [ 'enableAll' ],
    },
  },
};

const machineOptions = {
  actions: {
    sendDataToParent: sendParent((context, event) => event),
    disableAll: actions.pure(() => CHILDREN_TO_DISABLED.map((child) => send({ type: 'DISABLE' }, { to: child, delay: 1 }))),
    enableAll: actions.pure(() => CHILDREN_TO_DISABLED.map((child) => send({ type: 'ENABLE' }, { to: child }))),
    cleanErrorOnInputs: actions.pure(() => INPUTS_CHILDREN.map((input) => send({ type: 'CLEAN_ERROR' }, { to: input }))),
    showErrorOnInputs: actions
      .pure((context, event) => schemaValidator
        .validateFilterData(event.data)
        .map((errorMessage) => {
          const targetInput = INPUTS_CHILDREN.filter((input) => errorMessage.includes(input.replace(/.(Input||Dropdown)$/, '')))[0];
          return send(
            { type: 'SHOW_ERROR', data: { error: errorMessage } },
            { to: targetInput, delay: 1 },
          );
        })),
  },
  guards: {
    dataIsValid: (context, event) => !Array.isArray(schemaValidator.validateFilterData(event.data)),
  },
};

export default createMachine(machineDefinition, machineOptions);