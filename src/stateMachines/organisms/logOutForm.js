import { createMachine } from 'xstate';
import { assign } from '@xstate/immer';
import buttonMachine from '@stateMachines/atoms/button';
import setCookie from '@utils/setCookie';

export const machineDefinition = {
  id: 'logOutForm',
  initial: 'idle',
  context: {
    error: {},
  },
  invoke: [
    {
      id: 'logOutButton',
      src: buttonMachine,
    },
  ],
  states: {
    idle: {
      on: {
        PROCESS: {
          target: 'processing',
        },
      },
    },
    processing: {
      invoke: [
        {
          id: 'signOut',
          src: 'signOut',
          onDone: {
            target: 'finished',
          },
          onError: {
            actions: [ 'saveError' ],
            target: 'failed',
          },
        },
      ],
    },
    finished: {
      after: {
        1: {
          target: 'idle',
        },
      },
    },
    failed: {
      on: {
        PROCESS: {
          target: 'processing',
        },
      },
    },
  },
};

const machineOptions = {
  actions: {
    saveError: assign((context, event) => { context.error = event.data; }),
  },
  services: {
    signOut: () => new Promise((resolve, reject) => {
      try {
        setCookie('token', '');
        resolve();
      } catch (error) {
        reject(error);
      }
    }),
  },
};

export default createMachine(machineDefinition, machineOptions);