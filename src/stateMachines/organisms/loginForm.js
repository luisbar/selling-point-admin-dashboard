import { createMachine, send, sendParent, actions } from 'xstate';
import { assign } from '@xstate/immer';
import buttonMachine from '@stateMachines/atoms/button';
import inputMachine from '@stateMachines/atoms/input';
import schemaValidator from '@utils/schemaValidator';
import getItemFromCookies from '@utils/getItemFromCookies';
import setCookie from '@utils/setCookie';
import requester from '@utils/requester';
import decodeJWT from 'jwt-decode';

const INPUTS_CHILDREN = [ 'usernameInput', 'passwordInput' ];
const CHILDREN_TO_DISABLED = [ ...INPUTS_CHILDREN, 'loginButton' ];

export const machineDefinition = {
  id: 'loginForm',
  initial: 'idle',
  context: {
    error: {},
  },
  invoke: [
    {
      id: 'usernameInput',
      src: inputMachine,
    },
    {
      id: 'passwordInput',
      src: inputMachine,
    },
    {
      id: 'loginButton',
      src: buttonMachine,
    },
  ],
  states: {
    idle: {
      on: {
        '': {
          cond: 'userIsLogged',
          actions: [ 'saveSessionFromCookies' ],
          target: 'finished',
        },
        PROCESS: [
          {
            cond: 'dataIsValid',
            actions: [ 'cleanErrorOnInputs' ],
            target: 'processing',
          },
          {
            actions: [ 'cleanErrorOnInputs', 'showErrorOnInputs' ],
            target: 'idle',
          },
        ],
      },
    },
    processing: {
      entry: [ 'disableAll' ],
      invoke: [
        {
          id: 'signIn',
          src: 'signIn',
          onDone: {
            actions: [ 'sendDataToParent' ],
            target: 'finished',
          },
          onError: {
            actions: [ 'saveError' ],
            target: 'failed',
          },
        },
      ],
      exit: [ 'enableAll' ],
    },
    finished: {
      on: {
        PROCESS: {
          target: 'processing',
        },
      },
    },
    failed: {
      after: {
        1: {
          target: 'idle',
        },
      },
    },
  },
};

const machineOptions = {
  actions: {
    disableAll: actions.pure(() => CHILDREN_TO_DISABLED.map((child) => send({ type: 'DISABLE' }, { to: child, delay: 1 }))),
    enableAll: actions.pure(() => CHILDREN_TO_DISABLED.map((child) => send({ type: 'ENABLE' }, { to: child }))),
    sendDataToParent: sendParent((context, event) => ({ type: 'SAVE_SESSION', data: decodeJWT(event.data) })),
    saveError: assign((context, event) => { context.error = event.data; }),
    cleanErrorOnInputs: actions.pure(() => INPUTS_CHILDREN.map((input) => send({ type: 'CLEAN_ERROR' }, { to: input }))),
    saveSessionFromCookies: sendParent(() => ({ type: 'SAVE_SESSION', data: decodeJWT(getItemFromCookies('token')) })),
    showErrorOnInputs: actions
      .pure((context, event) => schemaValidator
        .validateSessionData(event.data)
        .map((errorMessage) => {
          const targetInput = INPUTS_CHILDREN.filter((input) => errorMessage.includes(input.replace('Input', '')))[0];
          return send(
            { type: 'SHOW_ERROR', data: { error: errorMessage } },
            { to: targetInput, delay: 1 },
          );
        })),
  },
  guards: {
    dataIsValid: (context, event) => !Array.isArray(
      schemaValidator.validateSessionData(event.data),
    ),
    userIsLogged: () => getItemFromCookies('token'),
  },
  services: {
    signIn: async (context, { data }) => {
      try {
        const { username, password } = schemaValidator.validateSessionData(data);
        const response = await requester.post('/auth/token', {
          username,
          password,
          grantType: 'password',
        });
        setCookie('token', response.data.accessToken);
        return response.data.accessToken;
      } catch (error) {
        throw error.response.data;
      }
    },
  },
};

export default createMachine(machineDefinition, machineOptions);