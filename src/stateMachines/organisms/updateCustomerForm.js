import { createMachine, send, actions } from 'xstate';
import { assign } from '@xstate/immer';
import buttonMachine from '@stateMachines/atoms/button';
import inputMachine from '@stateMachines/atoms/input';
import checkboxMachine from '@stateMachines/atoms/checkbox';
import schemaValidator from '@utils/schemaValidator';
import requester from '@utils/requester';

const INPUTS_CHILDREN = [ 'firstNameInput', 'lastNameInput', 'emailInput', 'nitInput' ];
const CHILDREN_TO_DISABLED = [ ...INPUTS_CHILDREN, 'saveButton' ];

export const machineDefinition = {
  id: 'updateCustomerForm',
  initial: 'idle',
  context: {
    error: {},
  },
  invoke: [
    {
      id: 'firstNameInput',
      src: inputMachine,
    },
    {
      id: 'lastNameInput',
      src: inputMachine,
    },
    {
      id: 'emailInput',
      src: inputMachine,
    },
    {
      id: 'nitInput',
      src: inputMachine,
    },
    {
      id: 'enabledCheckbox',
      src: checkboxMachine,
    },
    {
      id: 'saveButton',
      src: buttonMachine,
    },
  ],
  states: {
    idle: {
      on: {
        PROCESS: [
          {
            cond: 'dataIsValid',
            actions: [ 'cleanErrorOnInputs' ],
            target: 'processing',
          },
          {
            actions: [ 'cleanErrorOnInputs', 'validateAndShowErrorOnInputs' ],
            target: 'idle',
          },
        ],
      },
    },
    processing: {
      entry: [ 'disableAll' ],
      invoke: [
        {
          id: 'updateCustomer',
          src: 'updateCustomer',
          onDone: {
            actions: [ 'cleanErrorOnInputs' ],
            target: 'finished',
          },
          onError: {
            actions: [ 'saveError', 'showBackendErrorsOnInputs' ],
            target: 'failed',
          },
        },
      ],
      exit: [ 'enableAll' ],
    },
    finished: {
      after: {
        1: {
          target: 'idle',
        },
      },
    },
    failed: {
      after: {
        1: {
          target: 'idle',
        },
      },
    },
  },
};

const machineOptions = {
  actions: {
    disableAll: actions.pure(() => CHILDREN_TO_DISABLED.map((child) => send({ type: 'DISABLE' }, { to: child, delay: 1 }))),
    enableAll: actions.pure(() => CHILDREN_TO_DISABLED.map((child) => send({ type: 'ENABLE' }, { to: child }))),
    saveError: assign((context, event) => { context.error = event.data; }),
    cleanErrorOnInputs: actions.pure(() => INPUTS_CHILDREN.map((input) => send({ type: 'CLEAN_ERROR' }, { to: input }))),
    validateAndShowErrorOnInputs: actions
      .pure((context, event) => schemaValidator
        .validateUpdateCustomerData(event.data)
        .map((errorMessage) => {
          const targetInput = INPUTS_CHILDREN.filter((input) => errorMessage.includes(input.replace(/.(Input||Checkbox)$/, '')))[0];
          return send(
            { type: 'SHOW_ERROR', data: { error: errorMessage } },
            { to: targetInput, delay: 1 },
          );
        })),
    showBackendErrorsOnInputs: actions
      .pure(({ error: { fields } }) => Object.entries(fields)
        .map(([ field, errorMessage ]) => {
          const targetInput = INPUTS_CHILDREN.filter((input) => field.includes(input.replace(/.(Input||Checkbox)$/, '')))[0];
          return send(
            { type: 'SHOW_ERROR', data: { error: errorMessage } },
            { to: targetInput, delay: 1 },
          );
        })),
  },
  guards: {
    dataIsValid: (context, event) => !Array.isArray(
      schemaValidator.validateUpdateCustomerData(event.data),
    ),
  },
  services: {
    updateCustomer: async (context, { data }) => {
      try {
        const { id, ...others } = schemaValidator.validateUpdateCustomerData(data);
        const valuesAsObjects = Object.entries(others)
          .reduce((acc, [ key, value ]) => { acc[key] = { set: value }; return acc; }, {});
        const graphqlQuery = {
          query: `mutation updateCustomer($customerUpdateInput: CustomerUpdateInput!, $id: Int!) {
            updateCustomer(customerUpdateInput: $customerUpdateInput, id: $id) {
              id
            }
          }`,
          variables: {
            customerUpdateInput: { ...valuesAsObjects },
            id,
          },
        };
        const response = await requester.post(`/api/graphql`, graphqlQuery);
        return response.data.data.updateCustomer;
      } catch (error) {
        throw error.response.data.errors[0];
      }
    },
  },
};

export default createMachine(machineDefinition, machineOptions);