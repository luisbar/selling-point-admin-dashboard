import { createMachine } from 'xstate';
import createCategoryForm from '@stateMachines/organisms/createCategoryForm';

export const machineDefinition = {
  id: 'createCategory',
  initial: 'idle',
  invoke: [
    {
      id: 'createCategoryForm',
      src: createCategoryForm,
    },
  ],
  states: {
    idle: {
    },
  },
};

const machineOptions = {};

export default createMachine(machineDefinition, machineOptions);