import { createMachine } from 'xstate';
import createCustomerForm from '@stateMachines/organisms/createCustomerForm';

export const machineDefinition = {
  id: 'createCustomer',
  initial: 'idle',
  invoke: [
    {
      id: 'createCustomerForm',
      src: createCustomerForm,
    },
  ],
  states: {
    idle: {
    },
  },
};

const machineOptions = {};

export default createMachine(machineDefinition, machineOptions);