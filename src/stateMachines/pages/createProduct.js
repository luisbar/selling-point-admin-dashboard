import { actions, createMachine, send } from 'xstate';
import createProductForm from '@stateMachines/organisms/createProductForm';
import { assign } from '@xstate/immer';
import requester from '@utils/requester';

const CHILDREN_TO_DISABLED = [ 'createProductForm' ];

export const machineDefinition = {
  id: 'createProduct',
  initial: 'idle',
  context: {
    enabledCategories: [],
    error: {},
  },
  invoke: [
    {
      id: 'createProductForm',
      src: createProductForm,
    },
  ],
  states: {
    idle: {
      on: {
        PROCESS: [
          {
            target: 'processing',
          },
        ],
      },
    },
    processing: {
      entry: [ 'disableAll' ],
      invoke: [
        {
          id: 'getEnabledCategories',
          src: 'getEnabledCategories',
          onDone: {
            actions: [ 'saveEnabledCategories' ],
            target: 'finished',
          },
          onError: {
            actions: [ 'saveError' ],
            target: 'failed',
          },
        },
      ],
      exit: [ 'enableAll' ],
    },
    finished: {
      on: {
        PROCESS: [
          {
            target: 'processing',
          },
        ],
      },
    },
    failed: {
      after: {
        1: {
          target: 'idle',
        },
      },
    },
  },
};

const machineOptions = {
  actions: {
    disableAll: actions.pure(() => CHILDREN_TO_DISABLED.map((child) => send({ type: 'DISABLE' }, { to: child, delay: 1 }))),
    enableAll: actions.pure(() => CHILDREN_TO_DISABLED.map((child) => send({ type: 'ENABLE' }, { to: child }))),
    saveEnabledCategories: assign((context, event) => {
      context.enabledCategories = event.data;
    }),
    saveError: assign((context, event) => { context.error = event.data; }),
  },
  services: {
    getEnabledCategories: async () => {
      const args = {
        where: {
          enabled: {
            equals: true,
          },
        },
      };

      const graphqlQuery = {
        query: `query findCategories($args: FindManyCategoryArgs!) {
          findCategories(args: $args) {
            id
            name
          }
        }`,
        variables: {
          args,
        },
      };
      const response = await requester.post(`/api/graphql`, graphqlQuery);
      if (!response.data.data.findCategories.length) throw new Error('errors.response.empty');
      return response.data.data.findCategories.map((category) => ({
        key: category.id,
        value: category.name,
      }));
    },
  },
};

export default createMachine(machineDefinition, machineOptions);