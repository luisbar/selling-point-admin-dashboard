import { actions, createMachine, send } from 'xstate';
import createSaleForm from '@stateMachines/organisms/createSaleForm';
import { assign } from '@xstate/immer';
import requester from '@utils/requester';
import * as ramda from 'ramda';

const CHILDREN_TO_DISABLED = [ 'createSaleForm' ];

export const machineDefinition = {
  id: 'createSale',
  initial: 'idle',
  context: {
    enabledProducts: {},
    error: {},
  },
  invoke: [
    {
      id: 'createSaleForm',
      src: createSaleForm,
    },
  ],
  states: {
    idle: {
      on: {
        PROCESS: [
          {
            target: 'processing',
          },
        ],
      },
    },
    processing: {
      entry: [ 'disableAll' ],
      invoke: [
        {
          id: '',
          src: 'getEnabledProducts',
          onDone: {
            actions: [ 'saveEnabledProducts', 'spawnInputsOnCreateSaleForm' ],
            target: 'finished',
          },
          onError: {
            actions: [ 'saveError' ],
            target: 'failed',
          },
        },
      ],
      exit: [ 'enableAll' ],
    },
    finished: {
      on: {
        PROCESS: [
          {
            target: 'processing',
          },
        ],
      },
    },
    failed: {
      after: {
        1: {
          target: 'idle',
        },
      },
    },
  },
};

const machineOptions = {
  actions: {
    disableAll: actions.pure(() => CHILDREN_TO_DISABLED.map((child) => send({ type: 'DISABLE' }, { to: child, delay: 1 }))),
    enableAll: actions.pure(() => CHILDREN_TO_DISABLED.map((child) => send({ type: 'ENABLE' }, { to: child }))),
    saveEnabledProducts: assign((context, event) => {
      context.enabledProducts = event.data;
    }),
    saveError: assign((context, event) => { context.error = event.data; }),
    spawnInputsOnCreateSaleForm: actions.pure((context) => send({ type: 'SPAWN_INPUTS', data: context.enabledProducts }, { to: 'createSaleForm' })),
  },
  services: {
    getEnabledProducts: async () => {
      const args = {
        where: {
          enabled: {
            equals: true,
          },
        },
      };

      const graphqlQuery = {
        query: `query findProducts($args: FindManyProductArgs!) {
          findProducts(args: $args) {
            id
            name
            price
            category {
              name
            }
          }
        }`,
        variables: {
          args,
        },
      };
      const response = await requester.post(`/api/graphql`, graphqlQuery);
      if (!response.data.data.findProducts.length) throw new Error('errors.response.empty');
      return ramda.groupBy((product) => product.category.name)(response.data.data.findProducts);
    },
  },
};

export default createMachine(machineDefinition, machineOptions);