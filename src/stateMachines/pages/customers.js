import { actions, createMachine, send } from 'xstate';
import filterFormMachine from '@stateMachines/organisms/filterForm';
import paginatorMachine from '@stateMachines/molecules/paginator';
import { assign } from '@xstate/immer';
import requester from '@utils/requester';

const CHILDREN_TO_DISABLED = [ 'filterForm', 'paginator' ];
const DEFAULT_PAGE_NUMBER = 0;
const DEFAULT_PAGE_SIZE = '10';

export const machineDefinition = {
  id: 'customers',
  initial: 'idle',
  context: {
    customers: [],
    error: {},
    pagination: {
      pageNumber: DEFAULT_PAGE_NUMBER,
      pageSize: DEFAULT_PAGE_SIZE,
      direction: null,
    },
    filter: '',
  },
  invoke: [
    {
      id: 'filterForm',
      src: filterFormMachine,
    },
    {
      id: 'paginator',
      src: paginatorMachine,
    },
  ],
  states: {
    idle: {
      on: {
        PROCESS: [
          {
            target: 'processing',
          },
        ],
      },
    },
    processing: {
      entry: [ 'disableAll', 'updatePageNumber', 'updatePageSize', 'updateFilter', 'updateDirection' ],
      invoke: [
        {
          id: 'getCustomers',
          src: 'getCustomers',
          onDone: {
            actions: [ 'saveCustomers', 'updatePaginationOnPaginator' ],
            target: 'finished',
          },
          onError: {
            actions: [ 'saveError' ],
            target: 'failed',
          },
        },
      ],
      exit: [ 'enableAll' ],
    },
    finished: {
      on: {
        PROCESS: [
          {
            target: 'processing',
          },
        ],
      },
    },
    failed: {
      after: {
        1: {
          target: 'idle',
        },
      },
    },
  },
};

const machineOptions = {
  actions: {
    disableAll: actions.pure(() => CHILDREN_TO_DISABLED.map((child) => send({ type: 'DISABLE' }, { to: child }))),
    enableAll: actions.pure(() => CHILDREN_TO_DISABLED.map((child) => send({ type: 'ENABLE' }, { to: child }))),
    updatePageNumber: assign((context, { data: { pageNumber } = {} }) => {
      context.pagination.pageNumber = pageNumber;
    }),
    updatePageSize: assign((context, { data: { pageSize } = {} }) => {
      context.pagination.pageSize = pageSize || context.pagination.pageSize;
    }),
    updateFilter: assign((context, { data: { filter } }) => {
      if (filter !== undefined) context.filter = filter;
    }),
    updateDirection: assign((context, { data: { direction } }) => {
      context.pagination.direction = direction;
    }),
    updatePaginationOnPaginator: send(({ pagination }) => ({ type: 'UPDATE_PAGINATION', data: { pagination } }), { to: 'paginator' }),
    saveError: assign((context, event) => { context.error = event.data; }),
    saveCustomers: assign((context, event) => {
      context.customers = event.data;
    }),
  },
  services: {
    getCustomers: async ({
      customers,
      pagination: { pageNumber, pageSize, direction }, filter,
    }) => {
      let args = {
        take: Number(pageSize),
        skip: 0,
        orderBy: {
          id: 'desc',
        },
      };

      if (pageNumber) args = {
        ...args,
        cursor: {
          id: Number(direction === 'NEXT' ? customers.slice(-1)[0].id : Number(customers[0].id) + (Number(pageSize) + 2)),
        },
        skip: 1,
      };
      if (filter) args = {
        ...args,
        where: {
          OR: [
            { firstName: { contains: filter } },
            { lastName: { contains: filter } },
            { nit: { contains: filter } },
            { email: { contains: filter } },
          ],
        },
      };

      const graphqlQuery = {
        query: `query findCustomers($args: FindManyCustomerArgs!) {
          findCustomers(args: $args) {
            id
            firstName
            lastName
            email
            nit
            enabled
            createdAt
            createdBy {
              firstName
            }
            updatedAt
            updatedBy {
              firstName
            }
          }
        }`,
        variables: {
          args,
        },
      };
      const response = await requester.post(`/api/graphql`, graphqlQuery);
      if (!response.data.data.findCustomers.length) throw new Error('errors.response.empty');
      return response.data.data.findCustomers;
    },
  },
};

export default createMachine(machineDefinition, machineOptions);