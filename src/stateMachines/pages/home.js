import { assign } from '@xstate/immer';
import { createMachine } from 'xstate';
import checkboxMachine from '@stateMachines/atoms/checkbox';
import dropdownMachine from '@stateMachines/atoms/dropdown';
import logOutFormMachine from '@stateMachines/organisms/logOutForm';
import salesByStatusChartMachine from '@stateMachines/molecules/salesByStatusChart';
import saleAmountPerDayChartMachine from '@stateMachines/molecules/saleAmountPerDayChart';
import moment from 'moment';
import requester from '@utils/requester';

export const machineDefinition = {
  id: 'home',
  initial: 'idle',
  context: {
    monthlyRevenue: '0',
    weeklyRevenue: '0',
    dailyRevenue: '0',
    error: {},
  },
  invoke: [
    {
      id: 'changeThemeCheckbox',
      src: checkboxMachine,
    },
    {
      id: 'logOutForm',
      src: logOutFormMachine,
    },
    {
      id: 'languagesDropdown',
      src: dropdownMachine,
    },
    {
      id: 'salesByStatusChart',
      src: salesByStatusChartMachine,
    },
    {
      id: 'saleAmountPerDayChart',
      src: saleAmountPerDayChartMachine,
    },
  ],
  states: {
    idle: {
      on: {
        PROCESS: [
          {
            target: 'processing',
          },
        ],
        CHANGE_THEME: [
          {
            actions: [ 'changeTheme' ],
            target: 'processing',
          },
        ],
      },
    },
    processing: {
      invoke: {
        id: 'getRevenues',
        src: 'getRevenues',
        onDone: {
          actions: [ 'saveRevenues' ],
          target: 'finished',
        },
        onError: {
          actions: [ 'saveError' ],
          target: 'failed',
        },
      },
      on: {
        CHANGE_THEME: [
          {
            actions: [ 'changeTheme' ],
            target: 'processing',
          },
        ],
      },
    },
    finished: {
      on: {
        CHANGE_THEME: [
          {
            actions: [ 'changeTheme' ],
            target: 'finished',
          },
        ],
      },
    },
    failed: {
      after: {
        1: {
          target: 'idle',
        },
      },
    },
  },
};

const machineOptions = {
  actions: {
    changeTheme: () => document.documentElement.classList.toggle('dark'),
    saveError: assign((context, event) => { context.error = event.data; }),
    saveRevenues: assign((context, event) => {
      context.monthlyRevenue = event.data.monthlyRevenue;
      context.weeklyRevenue = event.data.weeklyRevenue;
      context.dailyRevenue = event.data.dailyRevenue;
    }),
  },
  services: {
    getRevenues: async () => {
      const monthlyQuery = {
        _sum: {
          total: true,
        },
        where: {
          OR: [
            {
              AND: [
                {
                  createdAt: {
                    gte: moment().startOf('month').toISOString(),
                  },
                },
                {
                  createdAt: {
                    lte: moment().endOf('month').toISOString(),
                  },
                },
                {
                  status: {
                    equals: 'completed',
                  },
                },
              ],
            },
          ],
        },
      };
      const weeklyQuery = {
        _sum: {
          total: true,
        },
        where: {
          OR: [
            {
              AND: [
                {
                  createdAt: {
                    gte: moment().startOf('week').toISOString(),
                  },
                },
                {
                  createdAt: {
                    lte: moment().endOf('week').toISOString(),
                  },
                },
                {
                  status: {
                    equals: 'completed',
                  },
                },
              ],
            },
          ],
        },
      };
      const dailyQuery = {
        _sum: {
          total: true,
        },
        where: {
          OR: [
            {
              AND: [
                {
                  createdAt: {
                    gte: moment().startOf('day').toISOString(),
                  },
                },
                {
                  createdAt: {
                    lte: moment().endOf('day').toISOString(),
                  },
                },
                {
                  status: {
                    equals: 'completed',
                  },
                },
              ],
            },
          ],
        },
      };
      const graphqlQuery = {
        query: `query aggregateSales ($args: SaleAggregateArgs!) {
          aggregateSales (args: $args) {
            _sum {
                total
            }
          }
        }`,
      };
      const [ monthlyResponse, weeklyResponse, dailyResponse ] = await Promise.all([
        requester.post(`/api/graphql`, { ...graphqlQuery, variables: { args: monthlyQuery } }),
        requester.post(`/api/graphql`, { ...graphqlQuery, variables: { args: weeklyQuery } }),
        requester.post(`/api/graphql`, { ...graphqlQuery, variables: { args: dailyQuery } }),
      ]);

      return {
        monthlyRevenue: String(monthlyResponse.data.data.aggregateSales._sum.total || 0),
        weeklyRevenue: String(weeklyResponse.data.data.aggregateSales._sum.total || 0),
        dailyRevenue: String(dailyResponse.data.data.aggregateSales._sum.total || 0),
      };
    },
  },
};

export default createMachine(machineDefinition, machineOptions);