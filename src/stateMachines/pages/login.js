import { createMachine } from 'xstate';
import loginFormMachine from '@stateMachines/organisms/loginForm';
import { assign } from '@xstate/immer';

export const machineDefinition = {
  id: 'login',
  initial: 'idle',
  context: {
    user: null,
  },
  invoke: [
    {
      id: 'loginForm',
      src: loginFormMachine,
    },
  ],
  states: {
    idle: {
      on: {
        SAVE_SESSION: {
          actions: [ 'saveSession' ],
          target: 'idle',
        },
      },
    },
  },
};

const machineOptions = {
  actions: {
    saveSession: assign((context, event) => { context.user = event.data; }),
  },
};

export default createMachine(machineDefinition, machineOptions);