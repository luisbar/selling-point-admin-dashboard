import { actions, createMachine, send } from 'xstate';
import filterFormMachine from '@stateMachines/organisms/filterForm';
import paginatorMachine from '@stateMachines/molecules/paginator';
import saleCancelationDialogMachine from '@root/stateMachines/molecules/saleCancelationDialog';
import saleCompletionDialogMachine from '@root/stateMachines/molecules/saleCompletionDialog';
import { assign } from '@xstate/immer';
import requester from '@utils/requester';

const statuses = [ 'pending', 'completed', 'canceled' ];
const CHILDREN_TO_DISABLED = [ 'filterForm', 'paginator' ];
const DEFAULT_PAGE_NUMBER = 0;
const DEFAULT_PAGE_SIZE = '10';

export const machineDefinition = {
  id: 'sales',
  initial: 'idle',
  context: {
    sales: [],
    error: {},
    pagination: {
      pageNumber: DEFAULT_PAGE_NUMBER,
      pageSize: DEFAULT_PAGE_SIZE,
      direction: null,
    },
    filter: '',
  },
  invoke: [
    {
      id: 'filterForm',
      src: filterFormMachine,
    },
    {
      id: 'paginator',
      src: paginatorMachine,
    },
    {
      id: 'saleCancelationDialog',
      src: saleCancelationDialogMachine,
    },
    {
      id: 'saleCompletionDialog',
      src: saleCompletionDialogMachine,
    },
  ],
  states: {
    idle: {
      on: {
        PROCESS: [
          {
            target: 'processing',
          },
        ],
      },
    },
    processing: {
      entry: [ 'disableAll', 'updatePageNumber', 'updatePageSize', 'updateFilter', 'updateDirection' ],
      invoke: [
        {
          id: 'getSales',
          src: 'getSales',
          onDone: {
            actions: [ 'saveSales', 'updatePaginationOnPaginator' ],
            target: 'finished',
          },
          onError: {
            actions: [ 'saveError' ],
            target: 'failed',
          },
        },
      ],
      exit: [ 'enableAll' ],
    },
    finished: {
      on: {
        UPDATE_SALE: [
          {
            target: 'finished',
            actions: [ 'updateSale' ],
          },
        ],
        PROCESS: [
          {
            target: 'processing',
          },
        ],
      },
    },
    failed: {
      after: {
        1: {
          target: 'idle',
        },
      },
    },
  },
};

const machineOptions = {
  actions: {
    disableAll: actions.pure(() => CHILDREN_TO_DISABLED.map((child) => send({ type: 'DISABLE' }, { to: child }))),
    enableAll: actions.pure(() => CHILDREN_TO_DISABLED.map((child) => send({ type: 'ENABLE' }, { to: child }))),
    updatePageNumber: assign((context, { data: { pageNumber } = {} }) => {
      context.pagination.pageNumber = pageNumber;
    }),
    updatePageSize: assign((context, { data: { pageSize } = {} }) => {
      context.pagination.pageSize = pageSize || context.pagination.pageSize;
    }),
    updateFilter: assign((context, { data: { filter } }) => {
      if (filter !== undefined) context.filter = filter;
    }),
    updateDirection: assign((context, { data: { direction } }) => {
      context.pagination.direction = direction;
    }),
    updatePaginationOnPaginator: send(({ pagination }) => ({ type: 'UPDATE_PAGINATION', data: { pagination } }), { to: 'paginator' }),
    saveError: assign((context, event) => { context.error = event.data; }),
    saveSales: assign((context, event) => {
      context.sales = event.data;
    }),
    updateSale: assign((context, event) => {
      const { id, ...others } = event.data;
      const saleIndex = context.sales.findIndex((sale) => sale.id === id);
      context.sales[saleIndex] = { ...context.sales[saleIndex], ...others };
    }),
  },
  services: {
    getSales: async ({
      sales,
      pagination: { pageNumber, pageSize, direction }, filter,
    }) => {
      let args = {
        take: Number(pageSize),
        skip: 0,
        orderBy: {
          id: 'desc',
        },
      };

      if (pageNumber) args = {
        ...args,
        cursor: {
          id: Number(direction === 'NEXT' ? sales.slice(-1)[0].id : Number(sales[0].id) + (Number(pageSize) + 2)),
        },
        skip: 1,
      };
      if (filter) args = {
        ...args,
        where: {
          OR: [
            {
              customer: {
                is: {
                  firstName: { contains: filter },
                },
              },
            },
            {
              customer: {
                is: {
                  lastName: { contains: filter },
                },
              },
            },
          ],
        },
      };
      if (statuses.includes(filter)) args = {
        ...args,
        where: {
          status: {
            in: filter,
          },
        },
      };

      const graphqlQuery = {
        query: `query findSales($args: FindManySaleArgs!) {
          findSales(args: $args) {
            id
            customer {
              id
              firstName
              lastName
            }
            saleDetail {
              id
              quantity
              subtotal
              product {
                id
                name
                price
              }
            }
            status
            total
            createdAt
            createdBy {
              firstName
            }
            updatedAt
            updatedBy {
              firstName
            }
          }
        }`,
        variables: {
          args,
        },
      };
      const response = await requester.post(`/api/graphql`, graphqlQuery);
      if (!response.data.data.findSales.length) throw new Error('errors.response.empty');
      return response.data.data.findSales.map((sale) => ({
        ...sale,
        customerName: `${sale.customer.firstName} ${sale.customer.lastName}`,
      }));
    },
  },
};

export default createMachine(machineDefinition, machineOptions);