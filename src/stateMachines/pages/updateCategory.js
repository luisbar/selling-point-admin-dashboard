import { createMachine } from 'xstate';
import updateCategoryForm from '@stateMachines/organisms/updateCategoryForm';

export const machineDefinition = {
  id: 'updateCategory',
  initial: 'idle',
  invoke: [
    {
      id: 'updateCategoryForm',
      src: updateCategoryForm,
    },
  ],
  states: {
    idle: {
    },
  },
};

const machineOptions = {};

export default createMachine(machineDefinition, machineOptions);