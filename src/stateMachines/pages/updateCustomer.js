import { createMachine } from 'xstate';
import updateCustomerForm from '@stateMachines/organisms/updateCustomerForm';

export const machineDefinition = {
  id: 'updateCustomer',
  initial: 'idle',
  invoke: [
    {
      id: 'updateCustomerForm',
      src: updateCustomerForm,
    },
  ],
  states: {
    idle: {
    },
  },
};

const machineOptions = {};

export default createMachine(machineDefinition, machineOptions);