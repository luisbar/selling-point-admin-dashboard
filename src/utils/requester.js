import axios from 'axios';
import getItemFromCookies from '@utils/getItemFromCookies';

class Requester {
  constructor() {
    this.axios = axios.create({
      baseURL: process.env.BASE_URL,
    });
  }

  get(
    url,
  ) {
    return this.axios(
      {
        url,
        method: 'GET',
        headers: {
          Authorization: `Bearer ${getItemFromCookies('token')}`,
        },
      },
    )
      .catch((error) => {
        if (error.response.status !== 403) throw error;

        window.location.href = '/login';
      });
  }

  post(
    url, data,
  ) {
    return this.axios(
      {
        url,
        data,
        method: 'POST',
        headers: {
          Authorization: `Bearer ${getItemFromCookies('token')}`,
        },
      },
    )
      .catch((error) => {
        if (error.response.status !== 403) throw error;

        window.location.href = '/login';
      });
  }
}

export default new Requester();
