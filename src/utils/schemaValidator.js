import joi from 'joi';

const getErrorMessages = ({ schema, data }) => {
  const { error, value } = schema.validate(data, { abortEarly: false });

  if (!error) return value;

  return error
    .details
    .map((detail) => `error.${detail.path.join('.')}.${detail.type}`);
};

const validateSessionData = (data = {}) => {
  const schema = joi.object({
    username: joi.string()
      .required()
      .email({ minDomainSegments: 2, tlds: { allow: [ 'com', 'net', 'io' ] } }),
    password: joi.string()
      .required()
      .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),
  });

  return getErrorMessages({ schema, data });
};

const validateFilterData = (data = {}) => {
  const schema = joi.object({
    pageSize: joi.number()
      .required()
      .max(50),
    filter: joi.string()
      .allow('')
      .allow(null)
      .optional()
      .alphanum()
      .max(50),
    pageNumber: joi.any()
      .optional(),
  });

  return getErrorMessages({ schema, data });
};

const validateCreateCategoryData = (data = {}) => {
  const schema = joi.object({
    name: joi.string()
      .required()
      .min(3)
      .max(50)
      .regex(/^[a-zA-Z ,.ñÑ\u00C0-\u017F]*$/),
    description: joi.string()
      .optional()
      .max(300)
      .regex(/^[a-zA-Z ,.ñÑ\u00C0-\u017F]*$/),
  });

  return getErrorMessages({ schema, data });
};

const validateUpdateCategoryData = (data = {}) => {
  const schema = joi.object({
    id: joi.number()
      .required(),
    name: joi.string()
      .optional()
      .min(3)
      .max(50)
      .regex(/^[a-zA-Z ,.ñÑ\u00C0-\u017F]*$/),
    description: joi.string()
      .optional()
      .max(300)
      .regex(/^[a-zA-Z ,.ñÑ\u00C0-\u017F]*$/),
    enabled: joi.boolean()
      .required(),
  });

  return getErrorMessages({ schema, data });
};

const validateCreateProductData = (data = {}) => {
  const schema = joi.object({
    name: joi.string()
      .required()
      .min(3)
      .max(50)
      .regex(/^[a-zA-Z ,.ñÑ\u00C0-\u017F]*$/),
    description: joi.string()
      .optional()
      .max(300)
      .regex(/^[a-zA-Z ,.ñÑ\u00C0-\u017F]*$/),
    cost: joi.number()
      .max(999999)
      .min(1)
      .precision(2)
      .required(),
    price: joi.number()
      .max(999999)
      .min(1)
      .precision(2)
      .required(),
    category: joi.number()
      .required(),
  });

  return getErrorMessages({ schema, data });
};

const validateUpdateProductData = (data = {}) => {
  const schema = joi.object({
    id: joi.number()
      .required(),
    name: joi.string()
      .required()
      .min(3)
      .max(50)
      .regex(/^[a-zA-Z ,.ñÑ\u00C0-\u017F]*$/),
    description: joi.string()
      .optional()
      .max(300)
      .regex(/^[a-zA-Z ,.ñÑ\u00C0-\u017F]*$/),
    cost: joi.number()
      .max(999999)
      .min(1)
      .precision(2)
      .required(),
    price: joi.number()
      .max(999999)
      .min(1)
      .precision(2)
      .required(),
    category: joi.number()
      .required(),
    enabled: joi.boolean()
      .required(),
  });

  return getErrorMessages({ schema, data });
};

const validateCreateCustomerData = (data = {}) => {
  const schema = joi.object({
    firstName: joi.string()
      .required()
      .min(3)
      .max(100)
      .regex(/^[a-zA-Z ,.ñÑ\u00C0-\u017F]*$/),
    lastName: joi.string()
      .required()
      .min(3)
      .max(100)
      .regex(/^[a-zA-Z ,.ñÑ\u00C0-\u017F]*$/),
    email: joi.string()
      .optional()
      .allow('')
      .email({ minDomainSegments: 2, tlds: { allow: [ 'com', 'net', 'io', 'bo' ] } }),
    nit: joi.string()
      .allow('')
      .min(3)
      .max(100)
      .regex(/^[0-9]*$/)
      .optional(),
  });

  return getErrorMessages({ schema, data });
};

const validateUpdateCustomerData = (data = {}) => {
  const schema = joi.object({
    id: joi.number()
      .required(),
    firstName: joi.string()
      .required()
      .min(3)
      .max(100)
      .regex(/^[a-zA-Z ,.ñÑ\u00C0-\u017F]*$/),
    lastName: joi.string()
      .required()
      .min(3)
      .max(100)
      .regex(/^[a-zA-Z ,.ñÑ\u00C0-\u017F]*$/),
    email: joi.string()
      .optional()
      .allow('')
      .email({ minDomainSegments: 2, tlds: { allow: [ 'com', 'net', 'io', 'bo' ] } }),
    nit: joi.string()
      .allow('')
      .min(3)
      .max(100)
      .regex(/^[0-9]*$/)
      .optional(),
    enabled: joi.boolean()
      .required(),
  });

  return getErrorMessages({ schema, data });
};

const validateCreateSaleData = (data = {}) => {
  const schema = joi.object({
    customer: joi.number()
      .required()
      .min(1),
    total: joi.number()
      .min(1)
      .required(),
    saleDetail: joi.array()
      .items(joi.object({
        productId: joi.number()
          .required(),
        quantity: joi.number()
          .min(1)
          .required(),
        subtotal: joi.number()
          .min(1)
          .required(),
      })),
  });

  return getErrorMessages({ schema, data });
};

const validateUpdateSaleData = (data = {}) => {
  const schema = joi.object({
    id: joi.number()
      .required(),
    customer: joi.number()
      .required()
      .min(1),
    total: joi.number()
      .min(1)
      .required(),
    currentSaleDetail: joi.array()
      .items(joi.object({
        productId: joi.number()
          .required(),
        quantity: joi.number()
          .min(1)
          .required(),
        subtotal: joi.number()
          .min(1)
          .required(),
      })),
    previousSaleDetail: joi.array()
      .items(joi.number())
      .required(),
  });

  return getErrorMessages({ schema, data });
};

export default {
  validateSessionData,
  validateFilterData,
  validateCreateCategoryData,
  validateUpdateCategoryData,
  validateCreateProductData,
  validateUpdateProductData,
  validateCreateCustomerData,
  validateUpdateCustomerData,
  validateCreateSaleData,
  validateUpdateSaleData,
};