const colors = require('tailwindcss/colors');
const defaultTheme = require('tailwindcss/defaultTheme');

const generateTemplate = ({ number, prefix, measure }) => {
  let template = {};
  Array(number).fill(1).forEach((item, index) => template[`${prefix}-${index + 1}`] = `repeat(${index + 1}, ${measure})`);
  return template;
}

const generateStartAndEnd = ({ number }) => {
  let startAndEnd = {};
  Array(number).fill(1).forEach((item, index) => startAndEnd[String(index)] = String(index));
  return startAndEnd;
}

module.exports = {
  purge: ['./src/**/*.{js,jsx,mdx}', './public/index.html'],
  darkMode: 'class',
  theme: {
    colors: {
      ...colors,
      'primary': '#ED6E7A',
      'primary-light': '#F4A4AC',
      'primary-dark': '#E94959',
      'accent-100': '#ECC30B',
      'accent-200': '#4DAA57',
      'accent-300': '#47E5BC',
      'light-100': '#FFFFFF',
      'dark-100': '#343434',
      'dark-200': '#3F3F46',
      'dark-300': '#CACACA',
      'dark-400': '#FAFAFA',
      'error': '#F8A5A5',
      'success': '#86EFAC',
    },
    extend: {
      spacing: defaultTheme.spacing,
      borderRadius: defaultTheme.borderRadius,
      gridRowStart: {
        ...generateStartAndEnd({ number: 50 }),
      },
      gridRowEnd: {
        ...generateStartAndEnd({ number: 50 }),
      },
      gridColumnStart: {
        ...generateStartAndEnd({ number: 50 }),
      },
      gridColumnEnd: {
        ...generateStartAndEnd({ number: 50 }),
      },
      gridTemplateRows: {
        'side-menu': '30% repeat(10, 5%)',
        'main': '10% 80% 10%',
        'table': '0.5fr 0.5fr 6fr',
        ...generateTemplate({ number: 50, prefix: 'table-body', measure: '5rem' }),
      },
    },
  },
  variants: {
    extend: {
      transform: ['dark'],
      translate: ['dark'],
      cursor: ['disabled'],
    },
  },
  plugins: [
    require('@tailwindcss/forms'),
  ],
}
